
import React, { Component } from 'react'
import ReactTouchEvents from "react-touch-events";

export default class Touch1 extends Component {

  constructor(props) {
    super(props);

    this._onTouchStart = this._onTouchStart.bind(this);
    this._onTouchMove = this._onTouchMove.bind(this);
    this._onTouchEnd = this._onTouchEnd.bind(this);
    this.state = { swiped: false };
    this._swipe = {};
    this.minDistance = 10;
  }

  _onTouchStart(e) {
    const touch = e.touches[0];
    this._swipe = { x: touch.clientX };
    console.log(touch)
    // alert("touch start")
    this.setState({ swiped: false });
  }

  _onTouchMove(e) {
    if (e.changedTouches && e.changedTouches.length) {
      const touch = e.changedTouches[0];
      console.log(touch)
      // alert("Box move start")
      this._swipe.swiping = true;
    }
  }

  _onTouchEnd(e) {
    const touch = e.changedTouches[0];
    console.log(touch)
    // alert("Box move stop")
    const absX = Math.abs(touch.clientX - this._swipe.x);
    console.log(absX)
    if (this._swipe.swiping && absX > this.minDistance ) {
      this.props.onSwiped && this.props.onSwiped();
      this.setState({ swiped: true });
    }
    this._swipe = {};
  }
  swiped = () => {
    console.log("Swiped")    
  }

  handleTap () {
    console.log("you have taped me");

}

handleSwipe (direction) {

    switch (direction) {
        case "top":
        case "bottom":
        case "left":
        case "right":
        
            console.log(`you swiped ${direction}`)
    
    }
}
  render() {
    return (
  <div>
      <div  style={{width:100,height:100,backgroundColor:"red"}}
        // onTouchStart={this._onTouchStart}
        onTouchStart={this.swiped}
        onTouchMove={this._onTouchMove}
        onTouchEnd={this._onTouchEnd}>
        {`Component-${this.state.swiped ? 'swiped' : ''}`}Swipe Me
      </div>

      <ReactTouchEvents
      onTap={ this.handleTap.bind(this) }
      onSwipe={ this.handleSwipe.bind(this) }
      >
      <button>Tap me</button>
    </ReactTouchEvents>
    </div>
    );
  }

}