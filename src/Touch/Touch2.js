

import React, { useCallback, useState, useEffect, useRef } from "react";

const INITIAL_PERCENT = 10
const MAX_PERCENT = 90
export default function Touch2() {
  const containerRef = useRef()
  const barRef = useRef()
  const [info, setInfo] = useState({
    isSliding: false,
    currentPercent: INITIAL_PERCENT
  })
  
  function getPercent(clientX) {
    const dims = containerRef.current.getBoundingClientRect()
    console.log(dims)
    return ((clientX - dims.left) / dims.width) * 100
  }
  
  const onMouseDown = useCallback(e => {
    e.preventDefault()
    if (info.isSliding === true) return
    console.log('mouse down')
    const percent = getPercent(e.clientX || e.touches[0].clientX)
    console.log(percent)
    setInfo({
      isSliding: true,
      currentPercent: percent > MAX_PERCENT ? MAX_PERCENT : percent
    }, () => {
      barRef.current.style.transition = 'none'
    })
  }, [info.isSliding])
  
  const onMouseMove =  useCallback(e => {
    const percent = getPercent(e.clientX || e.touches[0].clientX)
    console.log(percent)
    setInfo(state => ({
      ...state,
      currentPercent: (percent > MAX_PERCENT) ? MAX_PERCENT : 
                      (percent < INITIAL_PERCENT) ? INITIAL_PERCENT : percent
    }))
  }, [])
  
  const onMouseUp =  useCallback(e => {
    e.preventDefault()
    console.log('mouse up')
    const percent = getPercent(e.clientX || e.changedTouches[0].clientX)
    console.log(percent)
    setInfo(state => ({
      ...state,
      isSliding: false,
      currentPercent: (percent > MAX_PERCENT) ? MAX_PERCENT : INITIAL_PERCENT
    }))
  }, [])
  
  // Resizing Width Animation starts only if mouse is up
  useEffect(() => {
    if (info.isSliding === false) {
      barRef.current.classList.add('slide')
    } else {
      barRef.current.classList.remove('slide')
    }
  }, [info.isSliding])
  
  // Animation for 100% compeleted
  useEffect(() => {
    if (info.isSliding === false && info.currentPercent >= MAX_PERCENT) {
      barRef.current.classList.add('complete')
    } else {
      barRef.current.classList.remove('complete')
    }
  }, [info.isSliding, info.currentPercent])
  
  // added 'passive -> false' to prevent touchstart fires twice on Mobile screen.
  useEffect(() => { 
    containerRef.current.addEventListener('touchstart', onMouseDown, { passive: false ,capture: true})
    return () => containerRef.current.removeEventListener('touchstart', onMouseDown, { passive: false })
  }, [])
  
  useEffect(() => {
    if (info.isSliding === true) {
      window.addEventListener('mousemove', onMouseMove);
      window.addEventListener('touchmove', onMouseMove);
      window.addEventListener('mouseup', onMouseUp);
      window.addEventListener('touchend', onMouseUp);
    }
    return () => {
      window.removeEventListener('mousemove', onMouseMove)
      window.removeEventListener('touchmove', onMouseMove);
      window.removeEventListener('mouseup', onMouseUp);;
      window.removeEventListener('touchend', onMouseUp);
    }
  }, [info.isSliding])
  return (
    <div >
      <div style={{width:100,height:100,backgroundColor:"red"}}>
        isSliding : {String(info.isSliding)} <br />
        currentPercent : {parseInt(info.currentPercent)}
        
      </div>
      <div  onMouseDown={onMouseDown} ref={containerRef} style={{width:100,height:100,backgroundColor:"yellow"}}>
        <div  ref={barRef} style={{width: `${info.currentPercent}%`}} />
        <div />
      </div>
    </div>
  )
}