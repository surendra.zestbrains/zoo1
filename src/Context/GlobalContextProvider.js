import React from "react";

export const Context = React.createContext();
export const StateProvider = ({ children }) => {
  const [matchedOrangutan, setMatchedOrangutan] = React.useState("");
  return (
    <Context.Provider value={{ matchedOrangutan, setMatchedOrangutan }}>
      {children}
    </Context.Provider>
  );
};
