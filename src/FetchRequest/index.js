import constant from '../Constant'

export const get_request = async (requestURL) => {
    try {
        const response = await fetch(constant.api_hostname + requestURL, {
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-request-authorization': constant.secure_request
            }
        })
        return response.json()
    } catch (error) {
        return error
    }
}

export const post_request_json = async (requestURL, payload) => {
    try {
        const rawResponse = await fetch(constant.api_hostname + requestURL, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-request-authorization': constant.secure_request
            },
            body: JSON.stringify(payload)
        });

        const content = await rawResponse.json();
    
        return content   
    } catch (error) {
        return error
    }
}

export const post_request_form = async (requestURL, payload) => {
    try {
        const formData = new FormData();
        payload.forEach((payloadInfo, index)=> {
            formData.append(index, payloadInfo[index]);
        })
    
        const response = await fetch(constant.api_hostname + requestURL, {
            method: 'POST',
            headers: {
                'Content-Type':'application/x-www-form-urlencoded',
                'x-request-authorization': constant.secure_request
            },
            body: formData
        })
    
        return response.json()   
    } catch (error) {
        return error
    }
}

export const update_request_json = async (requestURL, payload) => {
    try {
        const rawResponse = await fetch(constant.api_hostname +  requestURL, {
            method: 'UPDATE',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-request-authorization': constant.secure_request
            },
            body: JSON.stringify(payload)
        });
        const content = await rawResponse.json();
    
        return content   
    } catch (error) {
        return error
    }
}

export const update_request_form = async (requestURL, payload) => {
    try {
        const formData = new FormData();
        payload.forEach((payloadInfo, index)=> {
            formData.append(index, payloadInfo[index]);
        })

        const response = await fetch(constant.api_hostname + requestURL, {
            method: 'UPDATE',
            headers: {
                'Content-Type':'application/x-www-form-urlencoded',
                'x-request-authorization': constant.secure_request
            },
            body: formData
        })

        return response.json()   
    } catch (error) {
        return error
    }
}

export const delete_request_json = async (requestURL, payload) => {
    try {
        const rawResponse = await fetch(constant.api_hostname + requestURL, {
            method: 'DELETE',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-request-authorization': constant.secure_request
            },
            body: JSON.stringify(payload)
        });
        const content = await rawResponse.json();
    
        return content   
    } catch (error) {
        return error
    } 
}

export const delete_request_form = async (requestURL, payload=[]) => {
    try {
        const formData = new FormData();
        payload.forEach((payloadInfo, index)=> {
            formData.append(index, payloadInfo[index]);
        })

        const response = await fetch(constant.api_hostname + requestURL, {
            method: 'DELETE',
            headers: {
                'Content-Type':'application/x-www-form-urlencoded',
                'x-request-authorization': constant.secure_request
            },
            body: formData
        })

        return response.json()   
    } catch (error) {
        return error
    }
}