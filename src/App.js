import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { StateProvider } from './Context/GlobalContextProvider';
import HomePage from './Pages/HomePage';
import TrainerHomePage from './Pages/HomePage/TrainerHomepage';
import UserExperience from './Pages/Experience/UserExperience';
import TrainerExperience from './Pages/Experience/TrainerExperience';
import PersonalityQuiz from './Pages/PersonalityQuiz';
import Order from './Pages/Order';
import Paint from './Pages/Paint';
import Carousel from './Pages/Paint/Carousel';
import SendResults from './Pages/SendResults';
import Questions from './Pages/PersonalityQuiz/Questions';
import Match from './Pages/PersonalityQuiz/Match';
import OrderGame from './Pages/Order/OrderGame';
import Pong from './Pages/Pong';
import OrangutanPong from './Pages/Pong/OrangutanPong';
import MainRoute from './Pages'
import Painting from './Pages/Paint/Painting'
import OrangutanPainting from './Pages/Paint/OrangutanPainting'
import OrangutanOrder from './Pages/Order/OrangutanOrder'
import Touch1 from './Touch/Touch1'
import Touch2 from './Touch/Touch2'
import Touch3 from './Touch/Touch3'
function App() {
  return (
    <div className="App">
      <StateProvider>
        <Router>
          <Switch>
            <Route path="/" exact component={MainRoute} />
            <Route path="/user" exact component={HomePage} />
            <Route path="/trainer" exact component={TrainerHomePage} />
            <Route exact path="/user-experience" component={UserExperience} />
            <Route exact path="/trainer-experience" component={TrainerExperience} />
            <Route exact path="/personality-quiz" component={PersonalityQuiz} />
            <Route exact path="/order" render={(props)=><Order {...props} singlePlayer/>} />
            <Route exact path="/paint" component={Paint} />
            <Route exact path="/carousel" component={Carousel} />
            <Route exact path="/send-results" component={SendResults} />
            <Route exact path="/questions" component={Questions} />
            <Route exact path="/match" component={Match} />
            <Route exact path="/order" component={Order} />
            <Route exact path="/order-game" render={(props)=><OrderGame {...props} singlePlayer/>} />
            <Route exact path="/order-game-multiplayer" render={(props)=><OrderGame {...props}/>} />
            <Route exact path="/orangutan-order" render={(props)=><OrangutanOrder {...props}/>} />
            <Route exact path="/pong" component={Pong} />
            <Route exact path="/trainer-paint" render={(props)=><Painting trainer={true} {...props}/>} />
            <Route exact path="/orangutan-paint" component={OrangutanPainting} />
            <Route exact path='/orangutan-pong' component={OrangutanPong}/>
            <Route path="/touch1" exact component={Touch1} />
            <Route path="/touch2" exact component={Touch2} />
            <Route path="/touch3" exact component={Touch3} />
          
          </Switch>
        </Router>
      </StateProvider>
    </div>
  );
}

export default App