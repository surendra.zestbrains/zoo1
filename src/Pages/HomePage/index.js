import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Layout from "../../Components/Layout";
import { H2, H3, Body1 } from "../../Components/Fonts";

const Wrapper = styled.div`
  min-height: 100vh;
  height: max-content;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const LeafButton = styled.button`
  outline: 0;
  border: 0;
  cursor: pointer;
  width: 250px;
  margin: 0;
  padding: 0;
  border-radius: 50%;
  background: transparent;
`;
const Hand = styled.img`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 35%;
  margin: 0% 0% -5px 8%;
`;
const ButtonText = styled(Body1)`
  font-size: 3vw;
  text-align: center;
  color: white;
  font-weight: 800;
  line-height: 6vh;
`;
const Logo = styled.img`
  min-width: 10%;
  max-width: 15%;
`;
const Heading2 = styled(H2)`
  color: #00833d;
  margin: 0;
  font-size: 4.5vw;
  line-height: 10vh;
  margin-top: 20px;
`;
const Heading3 = styled(H3)`
  color: #00833d;
  margin: 0;
  font-size: 3.5vw;
  font-weight: 600;
  line-height: 8vh;
`;
const LeafButtonImage = styled.img`
  position: absolute;
  z-index: -1;
  margin-left: -6.5vw;
  margin-top: -1.5vw;
`;
const MarginAutoDiv = styled.div`
  margin: auto;
`;
const HomePage = () => {
  document.body.style.overflow = "hidden";
  return (
    <Layout fullLeaves grass grassVariant={0} background={1}>
      <Wrapper>
        <Logo src="zoo-logo.png" alt="zoo-logo" />
        <Heading2>ORANGUTAN</Heading2>
        <Heading3>EXPERIENCE</Heading3>
        <Link to="/user-experience">
          <LeafButton>
            <LeafButtonImage src="leaf.png" alt="leaf-button" width="13.5%" />
            <MarginAutoDiv>
              <ButtonText>Start</ButtonText>
            </MarginAutoDiv>
          </LeafButton>
        </Link>
        <Hand src="orangutan-hand.png" alt="orangutan-hand" />
      </Wrapper>
    </Layout>
  );
};

export default HomePage;
