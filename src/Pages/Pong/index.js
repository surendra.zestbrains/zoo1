import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import Ready from "../../Components/Ready";
import PongTable from '../../Components/PongTable'

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
`;

const Pong = () => {
  const [countdown, toggleCountdown] = React.useState(true);
  let background=1;
  if (countdown===false)
  background=2;
  return (
    <Layout
    grass={countdown ? true : false}
    grassVariant={countdown ? 0 : null}
    logo={countdown ? true : false}
    background={background}
  >
    <Wrapper>
    
        <div>
         <PongTable orangutanControls={false}/>
        </div>
    
    </Wrapper>
  </Layout>
  );
};
   {/*** 
    <Layout
      grass={countdown ? true : false}
      grassVariant={countdown ? 0 : null}
      logo={countdown ? true : false}
      background={background}
    >
      <Wrapper>
        {countdown ? (
          <Ready toggleCountdown={toggleCountdown} />
        ) : (
          <div>
           <PongTable orangutanControls={false}/>
          </div>
        )}
      </Wrapper>
    </Layout>
    */}
  
  

export default Pong;
