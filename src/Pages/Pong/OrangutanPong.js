import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import Ready from "../../Components/Ready";
import PongTable from "../../Components/PongTable";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
`;

const OrangutanPong = () => {
  const [countdown, toggleCountdown] = React.useState(true);
  let background = 1;
  if (countdown === false) background = 2;
  return (
    <Layout
      grass={countdown ? true : false}
      grassVariant={countdown ? 0 : null}
      logo={countdown ? true : false}
      background={background}
      ReturnToMenuTop
    >
      <Wrapper>
        {countdown ? (
          <Ready toggleCountdown={toggleCountdown} />
        ) : (
          <div>
            <PongTable orangutanControls={true} />
          </div>
        )}
      </Wrapper>
    </Layout>
  );
};

export default OrangutanPong;
