import React from "react";
import Layout from "../../Components/Layout";
import styled from "styled-components";
import PaintBoard from "../../Components/PaintBoard";

const Wrapper = styled.div`
  display: flex;
  flew-wrap: wrap;
  flex-direction: row;
  width: 100vw;
  height: 100vh;
  justify-content: space-between;
`;
const YourPaintingContainer = styled.div`
  padding: 3vh 3vw 20vh 3vw;
  width: 100%;
`;
const PaletteColor = styled.img`
  position: absolute;
  left: ${(props) => `${props.left}`};
  top: ${(props) => `${props.top}`};
`;
const paletteColors = [
  { color: "palette-blue.png", left: "25vw", top: "82vh" },
  { color: "palette-red.png", left: "40vw", top: "82vh" },
  { color: "palette-orange.png", left: "55vw", top: "82vh" },
  { color: "palette-yellow.png", left: "70vw", top: "82vh" },
];
const Painting = () => {
  document.body.style.overflow = "hidden";
  const [selectedPaint, setSelectedPaint] = React.useState("palette-blue.png");
  return (
    <Layout background={0} ReturnToMenuBottom>
      <Wrapper>
        <YourPaintingContainer>
          <PaintBoard paintColor={selectedPaint} brushSize={"100px"} />
          {paletteColors.map((paint) => {
            return (
              <PaletteColor
                src={paint.color}
                left={paint.left}
                top={paint.top}
                alt="palette"
                width="12%"
                onClick={() => {
                  setSelectedPaint(paint.color);
                }}
              />
            );
          })}
        </YourPaintingContainer>
      </Wrapper>
    </Layout>
  );
};

export default Painting;
