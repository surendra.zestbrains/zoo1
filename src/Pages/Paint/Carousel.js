import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Layout from "../../Components/Layout";
import InfiniteCarousel from "react-leaf-carousel";
import { Body1 } from "../../Components/Fonts/index";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  flew-wrap: wrap;
  align-content: center;
`;
const MainCarouselDiv = styled.div`
  width: 140%;
  margin-top: 9%;
  margin-left: -20%;
  display: inline;
  align-content: center;
  justify-content: center;
`;

const LeafButton = styled.button`
  outline: 0;
  border: 0;
  cursor: pointer;
  width: 6%;
  margin-top: -2%;
  padding: 0;
  border-radius: 50%;
  background: transparent;
`;
const MarginAutoDiv = styled.div`
  margin: auto;
`;
const ButtonText = styled(Body1)`
  font-size: 1.2vw;
  text-align: center;
  color: white;
  font-weight: 800;
`;
const CarouselTag = styled(Body1)`
  font-size: 1.5vw;
  line-height:0;
  font-weight: 700;
  color: brown;
  margin-top: 6vh;
  padding-left: 20vw;
`;
const CarouselObjectDiv = styled.div`
  width: 25vw;
  height: 58vh;
`;

const CarouselFrame = styled.img`
  position: absolute;
  width:20vw;
`;
const CarouselImage = styled.img`
  margin-left: 11vw;
  margin-top: 9%;
  width:20vw;
  height: 40vh;
  overflow:hidden;
`;
const CarouselNextArrow = styled.img`
  position: absolute;
  right: 0;
  margin-right: 39%;
  margin-top: -1%;
  width: 6%;
`;
const CarouselPrevArrow = styled.img`
  position: absolute;
  width: 6%;
  bottom: 0;
  margin-bottom: -2.75%;
  margin-left: 38%;
  left: 0;
`;
const LeafButtonImage = styled.img`
  position: absolute;
  z-index: -1;
  margin-left: -6%;
  margin-top: -2%;
`;

const carouselNames = ["Blaze", "Chantek", "Pelari"];
const carouselImages = [
  "carousel-art-1.png",
  "carousel-art-2.png",
  "carousel-art-3.png",
];

const Carousel = () => {
  document.body.style.overflow = "hidden";
  return (
    <Layout halfLeaves grass grassVariant={2} logo background={1}>
      <Wrapper>
        <MainCarouselDiv>
          <InfiniteCarousel
            showSides={true}
            sidesOpacity={0.8}
            sideSize={1}
            nextArrow={
              <CarouselNextArrow
                src="carousel-arrow.png"
                alt="carousel-arrow"
              />
            }
            prevArrow={
              <CarouselPrevArrow
                src="carousel-arrow-reverse.png"
                alt="carousel-arrow"
              />
            }
          >
            {carouselImages.map((value, index) => {
              return (
                <CarouselObjectDiv key={value}>
                  {/* <CarouselFrame
                    src="carousel-art-frame.png"
                    alt="carousel-art-frame"
                    width="2vw"
                  /> */}
                  <CarouselImage src={value} alt={value} />
                  <CarouselTag>{carouselNames[index]}</CarouselTag>
                </CarouselObjectDiv>
              );
            })}
          </InfiniteCarousel>
        </MainCarouselDiv>
        <Link to="/send-results">
          <LeafButton>
            <LeafButtonImage src="leaf.png" alt="leaf-button" width="12%" />
            <MarginAutoDiv>
              <ButtonText>Select as Favorite</ButtonText>
            </MarginAutoDiv>
          </LeafButton>
        </Link>
      </Wrapper>
    </Layout>
  );
};

export default Carousel;
