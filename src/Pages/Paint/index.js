import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { Link, Redirect } from "react-router-dom";
import ReactPlayer from "react-player";
const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const LinkButton = styled(Link)`
  width: 600px;
  position: absolute;
  text-align: center;
  margin: auto;
`;


const Frame = styled.img`
  margin-top: 3%;
  width: 700px;
`;

const ReactPlayerStyled = styled(ReactPlayer)`
  position: relative;
  margin-left: -3.5%;
`;

const Paint = () => {
  const [redirect, toggleRedirect] = React.useState(false);
  if (redirect) return <Redirect to="/carousel" />;
  else
    return (
      <Layout halfLeaves grass grassVariant={1} logo background={1}>
        <Wrapper>
          <Frame src="video_frame.png" alt="wood-frame" />
          <LinkButton to="/carousel">
            <ReactPlayerStyled
              height={"40%"}
              playing
              onEnded={() => {
                toggleRedirect(!redirect);
              }}
              url={"https://zoo-atlanta-new.s3.ap-south-1.amazonaws.com/art-by-apes-video.mp4"}
            />
          </LinkButton>
        </Wrapper>
      </Layout>
    );
};

export default Paint;
