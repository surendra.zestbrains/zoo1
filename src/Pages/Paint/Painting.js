import React from "react";
import Layout from "../../Components/Layout";
import styled from "styled-components";
import PaintBoard from "../../Components/PaintBoard";
import { Body1 } from "../../Components/Fonts/index";
import { Redirect } from "react-router-dom";

const Wrapper = styled.div`
  display: flex;
  flew-wrap: wrap;
  flex-direction: row;
  width: 100vw;
  height: 100vh;
`;
const LeftContainer = styled.div``;

const PaintingFrame = styled.img`
  position: absolute;
  right: 5vw;
  top: 20vh;
  z-index: -1;
  overflow: hidden;
`;

const OrangutanViewContainer = styled.div`
  position: absolute;
  width: 25vw;
  margin-left: 6vw;
  margin-top: 12vh;
`;
const PalleteHolderContainer = styled.div``;
const YourPaintingContainer = styled.div`
  width: 52vw;
  height: 50vh;
  margin-left: 40vw;
  margin-top: 24vh;
`;
const PaletteContainerUndoButton = styled(Body1)`
  z-index: 1000;
  font-size: 1.8vw;
  font-weight: 600;
  color: white;
  cursor: pointer;
  position: absolute;
  left: 5vw;
  top: 49vh;
  margin-left: 4%;
  margin-top: 12.5%;
`;
const PaletteContainerFinishButton = styled(Body1)`
  z-index: 1000;
  font-size: 1.8vw;
  font-weight: 600;
  color: white;
  cursor: pointer;
  position: absolute;
  left: 5vw;
  top: 49vh;
  margin-left: 11%;
  margin-top: 12.5%;
`;

const LeafImg = styled.img`
  position: absolute;
  left: 5vw;
  top: 48vh;
`;

const PaletteColor = styled.img`
  cursor: pointer;
  position: absolute;
  left: ${(props) => `${props.left}`};
  top: ${(props) => `${props.top}`};
  margin-top: ${(props) => `${props.marginTop}`};
  margin-left: ${(props) => `${props.marginLeft}`};
`;
const paletteColors = [
  {
    color: "palette-blue.png",
    left: "5vw",
    top: "50vh",
    marginLeft: "14%",
    marginTop: "1%",
  },
  {
    color: "palette-red.png",
    left: "5vw",
    top: "50vh",
    marginLeft: "6%",
    marginTop: "2%",
  },
  {
    color: "palette-orange.png",
    left: "5vw",
    top: "50vh",
    marginLeft: "13%",
    marginTop: "6%",
  },
  {
    color: "palette-yellow.png",
    left: "5vw",
    top: "50vh",
    marginLeft: "5%",
    marginTop: "7%",
  },
];
const Painting = ({ trainer }) => {
  document.body.style.overflow = "hidden";
  const [selectedPaint, setSelectedPaint] = React.useState("palette-blue.png");
  const [resetPaint, setResetPaint] = React.useState(false);
  const [finishPainting, setFinishPainting] = React.useState(false);
  return (
    <Layout grass grassVariant={0} logo trainer background={1}>
      {finishPainting ? <Redirect to="/send-results" /> : null}
      <Wrapper>
        <LeftContainer>
          <OrangutanViewContainer>
            <img
              src="painting-frame-small.png"
              alt="painting-frame-small"
              width="100%"
            />
          </OrangutanViewContainer>
          <PalleteHolderContainer>
            <PaletteContainerUndoButton
              onClick={() => {
                setResetPaint(true);
              }}
            >
              Undo
            </PaletteContainerUndoButton>
            <PaletteContainerFinishButton
              onClick={() => {
                setFinishPainting(true);
              }}
            >
              Finish
            </PaletteContainerFinishButton>
            <LeafImg src="leaf-large.png" alt="leaf-large" width="25%" />
          </PalleteHolderContainer>
          {paletteColors.map((paint) => {
            return (
              <PaletteColor
                src={paint.color}
                left={paint.left}
                top={paint.top}
                marginLeft={paint.marginLeft}
                marginTop={paint.marginTop}
                alt="palette"
                width="6%"
                onClick={() => {
                  setSelectedPaint(paint.color);
                }}
              />
            );
          })}
        </LeftContainer>
        <YourPaintingContainer>
          <PaintingFrame
            src="painting-frame-large.png"
            alt="painting-frame-large"
            width="58%"
            height="58%"
          />
          <PaintBoard
            paintColor={selectedPaint}
            brushSize={"60px"}
            resetPaint={resetPaint}
            setResetPaint={setResetPaint}
          />
        </YourPaintingContainer>
      </Wrapper>
    </Layout>
  );
};

export default Painting;
