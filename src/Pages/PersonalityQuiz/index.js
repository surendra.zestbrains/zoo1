import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { H2 } from "../../Components/Fonts";
import { Link, Redirect } from "react-router-dom";
import ReactPlayer from "react-player";
const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const Heading = styled(H2)`
  position: absolute;
  left: 0;
  right: 0;
  color: #215da6;
  font-size: 32px;
`;
const LinkButton = styled(Link)`
  postion: absolute;
  width: 600px;
  position: absolute;
  text-align: center;
  margin: auto;
`;
const Art = styled.img`
  width: 600px;
  position: absolute;
  text-align: center;
  margin: -30% 0% 0% -50%;
`;

const Frame = styled.img`
  margin-top: 3%;
  width: 700px;
`;

const ReactPlayerStyled = styled(ReactPlayer)`
  position: relative;
  margin-left: -3.5%;
`;

const PersonalityQuiz = () => {
  document.body.style.overflow = "hidden";
  const [redirect, toggleRedirect] = React.useState(false);
  if (redirect) return <Redirect to="/questions" />;
  else
    return (
      <Layout grass grassVariant={2} logo orangutanSitting background={1}>
        <Heading>Do Orangutans have personalities?</Heading>
        <Wrapper>
          <Frame src="video_frame.png" alt="wood-frame" />
          <LinkButton to="/questions">
            <ReactPlayerStyled
              height={"40%"}
              onEnded={() => {
                toggleRedirect(!redirect);
              }}
              playing
              url="https://zoo-atlanta-new.s3.ap-south-1.amazonaws.com/which-orangutan-are-you-video.mp4"
            />
          </LinkButton>
        </Wrapper>
      </Layout>
    );
};

export default PersonalityQuiz;
