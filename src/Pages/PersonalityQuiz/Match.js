import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { Body1 } from "../../Components/Fonts";
import { Link } from "react-router-dom";
import { Context } from "../../Context/GlobalContextProvider";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const TopText = styled(Body1)`
  color: #215da6;
  margin-top: 4%;
  font-size: 2vw;
  font-weight: 600;
`;
const OrangutanName = styled(Body1)`
  color: #ffffff;
  font-size: 4vw;
  font-weight: 800;
  margin-top:-2%;
`;
const LeafButton = styled.button`
  outline: 0;
  border: 0;
  cursor: pointer;
  width: 30%;
  margin: 0;
  padding: 0;
  margin-right: 10%;
  margin-left: 10%;
  border-radius: 60%;
  background: transparent;
`;

const LeafImg = styled.img`
  position: absolute;
  z-index: -1;
  margin-left: -7%;
  margin-top: -2.5%;
`;
const ButtonText = styled(Body1)`
  font-size: 2vw;
  line-height: 2vw;
  text-align: center;
  color: white;
  font-weight: 800;
`;

const Frame = styled.img`
  margin-top: 12%;
  width: 32vw;
  height: 23vw;
  position: absolute;
`;
const Art = styled.img`
  margin-top: 13.5%;
  width: 30%;
  overflow: hidden;
  position: absolute;
  text-align: center;
`;
const ButtonsDiv = styled.div`
  margin-top:42vh;
`

const Match = () => {
  const state = React.useContext(Context);
  document.body.style.overflow = "hidden";
  return (
    <Layout grass grassVariant={0} logo orangutanBehindGrass background={1}>
      <Wrapper>
        <TopText>You're a match to</TopText>
        <OrangutanName>{state.matchedOrangutan}</OrangutanName>
        <Frame src="orangutan-match-frame.png" alt="wood-frame" />
        <Art src={`orangutan_${state.matchedOrangutan.toLowerCase()}.jpg`} alt={state.matchedOrangutan.toLowerCase()} />
        <ButtonsDiv>
          <Link to="send-results">
            <LeafButton>
              <LeafImg
                src="leaf-inverted.png"
                alt="leaf-button"
                width="15%"
              />
              <div style={{ margin: "auto" }}>
                <ButtonText>Send Results</ButtonText>
              </div>
            </LeafButton>
          </Link>
          <Link to="user">
            <LeafButton>
              <LeafImg
                src="leaf-inverted.png"
                alt="leaf-button"
                width="15%"
              />
              <div style={{ margin: "auto" }}>
                <ButtonText>Back to Home</ButtonText>
              </div>
            </LeafButton>
          </Link>
        </ButtonsDiv>
      </Wrapper>
    </Layout>
  );
};

export default Match;
