import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { H2, Body1 } from "../../Components/Fonts";
// import { Link } from "react-router-dom";
import { Context } from "../../Context/GlobalContextProvider";
import { Redirect } from "react-router-dom";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Heading = styled(H2)`
  left: 0;
  right: 0;
  margin-top: 10vh;
  color: #215da6;
  font-size: 2.6vw;
  line-height: 2.6vw;
`;

const QuestionsFrame = styled.div`
  position: absolute;
  margin-top: 15vh;
  width: 80vw;
  height: 28vh;
  overflow: hidden;
  align-items: center;
  justify-content: center;
`;

const QuestionsFrameBorder = styled.img`
  position: absolute;
  margin-top: 20vh;
  width: 65vw;
  height: 24vh;
  overflow: hidden;
`;

const QuestionH2 = styled(H2)`
  left: 0;
  right: 0;
  margin-top: 15vh;
  color: #00763f;
  font-size: 1.8vw;
  line-height: 1.8vw;
`;

const OptionsDiv = styled.div`
  margin-top: 30vh;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  width: 50%;
  height: 40%;
  justify-content: space-evenly;
`;

const OptionObjectDiv = styled.div`
  cursor: pointer;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  padding: 4%;
  align-items: center;
  justify-content: center;
  background-repeat: no-repeat;
  background-image: url(${(props) => props.image});
  background-position: center;
  background-size: contain;
`;

const QuestionOptions = styled(Body1)`
  margin-top: 0;
  margin-bottom: 0;
  pointer-events: none;
  color: #00763f;
  word-wrap: break-word;
  text-align: center;
  font-weight: bold;
  font-size: ${(props) => props.size};
`;

const QuestionsArray = [
  "How do you spend your free time?",
  "What is your favourite snack?",
  "How social are you?",
  "How good are you at solving difficult problems",
];

const optionsArray = [
  [
    "Playing on the computer",
    "Crafting Projects",
    "Relaxing curled on the couch",
    "Playing and Active",
    "Taking things apart to rebuild them",
  ],
  ["Grapes", "Banana", "Oatmeal", "Salad", "Everything"],
  [
    "I love meeting everyone",
    "I like my alone time",
    "I have 1-3 best friends that I like to hangout with",
  ],
  [
    "I accel at all challenges and figure things out quickly",
    "I have to study things and Eventually get the right answer",
    "I sometimes get the correct answer",
    "Getting the right answer is not important to me",
  ],
];

/**
BijiBABB
MaduAECA
RemyCCBD
KejuDBCB
SatuEDCA
PongoDAAC 
 */

const results = [
  {
    orangutanName: "Biji",
    answers: [1, 0, 1, 1],
    matches: 0,
  },
  {
    orangutanName: "Satu",
    answers: [4, 3, 2, 0],
    matches: 0,
  },
  {
    orangutanName: "Madu",
    answers: [0, 4, 2, 0],
    matches: 0,
  },
  {
    orangutanName: "Remy",
    answers: [2, 2, 1, 3],
    matches: 0,
  },
  {
    orangutanName: "Keju",
    answers: [3, 1, 2, 1],
    matches: 0,
  },
  {
    orangutanName: "Pongo",
    answers: [3, 0, 0, 2],
    matches: 0,
  },
];

const Questions = () => {
  const state = React.useContext(Context);
  const [selectedOrangutan, setSelectedOrangutan] = React.useState("");
  document.body.style.overflow = "hidden";
  const [currentQuestion, setCurrentQuestion] = React.useState(0);
  const [answersSelected, setAnswersSelected] = React.useState([]);
  const [questionsOver, setQuestionsOver] = React.useState(false);
  if (questionsOver === true) {
    return <Redirect to="/match" />;
  }
  return (
    <Layout halfLeaves grass grassVariant={0} orangutanStanding background={1}>
      <Wrapper>
        <QuestionsFrameBorder src={"questions-frame.png"} />
        <Heading>Question {currentQuestion + 1}</Heading>
        <QuestionsFrame>
          <QuestionH2>{QuestionsArray[currentQuestion]}</QuestionH2>
        </QuestionsFrame>
        <OptionsDiv>
          {currentQuestion > 3
            ? setQuestionsOver(true)
            : optionsArray[currentQuestion].map((question, index) => {
                if (currentQuestion !== 3) {
                  return (
                    <OptionObjectDiv
                      width="20%"
                      height="26%"
                      image="answer-frame.png"
                      key={question}
                      onClick={() => {
                        let setOrangutan = "";
                        if (currentQuestion < QuestionsArray.length) {
                          setCurrentQuestion(currentQuestion + 1);
                          answersSelected.push(index)
                          setAnswersSelected(answersSelected);
                        }
                        if (currentQuestion === QuestionsArray.length - 1) {
                          if (typeof window !== "undefined") {
                            results.map((orangutan, oIndex) => {
                              answersSelected.map((answer, aIndex) => {
                                if (orangutan.answers[aIndex] === answer) {
                                  orangutan.matches++;
                                }
                              });
                            });
                            let highestCount = -1;
                            results.map((orangutan, oIndex) => {
                              if (orangutan.matches > highestCount) {
                                highestCount = orangutan.matches;
                                setOrangutan = orangutan.orangutanName;
                              }
                            });
                            state.setMatchedOrangutan(setOrangutan);
                            // if (typeof window !== `undefined`) { window.location = "/match";}
                          }
                        }
                      }}
                    >
                      <QuestionOptions size="1.0vw">{question}</QuestionOptions>
                    </OptionObjectDiv>
                  );
                } else
                  return (
                    <OptionObjectDiv
                      width="30%"
                      height="30%"
                      image="answer4-frame.png"
                      key={question}
                      onClick={() => {
                        let setOrangutan = "";
                        if (currentQuestion < QuestionsArray.length) {
                          setCurrentQuestion(currentQuestion + 1);
                          answersSelected.push(index)
                          setAnswersSelected(answersSelected);
                        }
                        if (currentQuestion === QuestionsArray.length - 1) {
                          if (typeof window !== "undefined") {
                            results.map((orangutan, oIndex) => {
                              answersSelected.map((answer, aIndex) => {
                                if (orangutan.answers[aIndex] === answer) {
                                  orangutan.matches++;
                                }
                              });
                            });

                            let highestCount = -1;
                            results.map((orangutan, oIndex) => {
                              if (orangutan.matches > highestCount) {
                                highestCount = orangutan.matches;
                                setOrangutan = orangutan.orangutanName;
                              }
                            });
                            
                            if(results[1].matches === highestCount && results[2].matches === highestCount){
                              state.setMatchedOrangutan(results[2].orangutanName);
                            }else{
                              state.setMatchedOrangutan(setOrangutan);
                            }
                            // console.log(setOrangutan);
                            // if (typeof window !== `undefined`) { window.location = "/match";}
                          }
                        }
                      }}
                    >
                      <QuestionOptions size="1vw">{question}</QuestionOptions>
                    </OptionObjectDiv>
                  );
              })}
        </OptionsDiv>
      </Wrapper>
    </Layout>
  );
};

export default Questions;
