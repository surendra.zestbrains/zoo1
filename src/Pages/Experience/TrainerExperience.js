import React from "react";
import { Link } from "react-router-dom";
import { H2, Body1 } from "../../Components/Fonts";
import styled from "styled-components";
import Layout from "../../Components/Layout";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Heading = styled(H2)`
  padding-top: 10vh;
  color: #215DA6;
  font-size: 4vw;
  line-height: 10vh;
`;

const LinkedBox = styled(Link)`
  text-decoration: none !important;
  text-transform: uppercase;
  width: 250px;
  font-weight: 600;
  width: 17vw;
`;

const ButtonLinkText = styled(Body1)`
  font-size: 2vw;
  width: 17vw;
  margin: 0 auto;
`;

const ButtonRow = styled.div`
  display: flex;
  flex-direction: row;
  gap: 5vw;
`;
const MenuButton = styled.img`
  width: 17vw;
  cursor: pointer;
`;


const menuButtons = ["pong-rect.svg", "order-rect.svg", "paint-rect.svg"];
const menuButtonTitles = [{
  text: "pong",
  color: "#EA0029",
  width: '13vw',
}, {
  text: "thinking apes",
  color: "#0031A0",
  width: '17vw',
}, {
  text: "art by apes",
  color: "#00833E",
  width: '13vw',
}];
const buttonLinks = ["pong", "order-game-multiplayer", "trainer-paint"];

const TrainerExperience = () => { 
  document.body.style.overflow = "hidden";
  return (
    <Layout grass grassVariant={0} logo orangutanBehindGrass trainer={true} background={1}>
      <Wrapper>
        <Heading>CHOOSE LIVE EXPERIENCE</Heading>
        <ButtonRow>
          {menuButtons.map((button, index) => (
            <LinkedBox key={button} to={buttonLinks[index]} style={{ color: [menuButtonTitles[index].color] }}>
              <MenuButton key={button} src={button} alt={button} />
              <ButtonLinkText style={{width: [menuButtonTitles[index].width] }}>{menuButtonTitles[index].text}</ButtonLinkText>
            </LinkedBox>
          ))}
        </ButtonRow>
        {/* <Orangutan src="orangutan-walking.png" alt="orangutan-image" /> */}
      </Wrapper>
    </Layout>
  );
};

export default TrainerExperience;
