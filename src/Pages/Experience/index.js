import React from "react";
import { Link } from "react-router-dom";
import { H2 } from "../../Components/Fonts";
import styled from "styled-components";
import Layout from "../../Components/Layout";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Heading = styled(H2)`
  padding-top: 1%;
  color: #215da6;
  font-size: 60px;
`;
const ButtonRow = styled.div`
  display: flex;
  flex-direction: row;
  gap: 80px;
`;
const MenuButton = styled.img`
  width: 250px;
  cursor: pointer;
`;
const Orangutan = styled.img`
  position: absolute;
  bottom: 0;
  left: 0;
  margin-left: -2.5%;
  width: 400px;
  transform: translate(-10%, 20%);
`;

const menuButtons = ["quiz-button.png", "order-button.png", "paint-button.png"];
const buttonLinks = ["/personality-quiz", "order", "paint"];

const Experience = () => {
  document.body.style.overflow = "hidden";
  return (
    <Layout grass grassVariant={0} logo orangutanStanding background={1}>
      <Wrapper>
        <Heading>CHOOSE YOUR EXPERIENCE</Heading>
        <ButtonRow>
          {menuButtons.map((button, index) => (
            <Link key={button} to={buttonLinks[index]}>
              <MenuButton key={button} src={button} alt={button} />
            </Link>
          ))}
        </ButtonRow>
        {/* <Orangutan src="orangutan-walking.png" alt="orangutan-image" /> */}
      </Wrapper>
    </Layout>
  );
};

export default Experience;
