import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Layout from "../../Components/Layout";
import { H2, H4, Body1 } from "../../Components/Fonts";
import Keyboard from "../../Components/Keyboard";
import { Context } from "../../Context/GlobalContextProvider";
import {post_request_json} from "../../FetchRequest"
import { useHistory } from "react-router-dom";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const Container = styled.div`
  display: flex;
  width: 70vw;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 50px;
`;
const Section = styled.div`
  display: flex;
  width: 47.5%;
  gap: 40px;
  text-align: center;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;
const Heading2 = styled(H2)`
  color: #00833d;
  margin: 0;
`;
const Heading4 = styled(H4)`
  color: #00833d;
  margin: 0;
`;
const Checkbox = styled.input`
  transform: scale(1.5);
  cursor: pointer;
  outline: 0;
  border: 0;
  margin-top:-1vh;
  &:focus {
    outline: 0;
  }
`;
const CheckboxHolder = styled.div`
  display: flex;
  flex-direction: row;
  gap: 1vw;
  padding-top: 0.5vh;
`;

const Heading5 = styled(H4)`
margin-top:1vh;
  font-size: 20px;
  line-height:0;
  font-weight: 600;
  color: #00833d;
  margin: 0;
`;
const Input = styled.input`
  width: 32vw;
  background: white;
  margin-top:2vh;
  border: 1px solid #00833d;
  color: #00833d;
  font-size: 24px;
  padding: 8px;
  outline: 0;
  &:focus {
    outline: 0;
  }
`;
const PrivacyNotice = styled.div`
  height: 30vh;
  background: lightyellow;
  overflow-x: hidden;
  overflow-y: scroll;
`;
const Para = styled.p`
  font-size: 10px;
  color: #000000;
`;

const Label = styled.label`
  font-family: LithosPro;
  font-size: 2vw;
  font-weight: 600;
  color: #00833d;
`;

const ButtonTextSmall = styled(Body1)`
  margin-top: 40px;
  font-size: 15px;
  text-align: center;
  color: white;
  font-weight: 800;
  padding: 5%;
  position: relative;
  top: -1.4vh;
  font-size: 1.2vh;
`;

const LeafButtonInverted = styled.button`
  background-image: url(leaf-inverted.png);
  background-size: 100% 100%;
  background-position: center center;
  background-repeat: no-repeat;
  outline: 0;
  border: 0;
  cursor: pointer;
  height: 12vh;
  width: 10vh;
  margin: 0;
  padding: 0;
  margin-top: -5vh;
  margin-left:3vw;
  margin-right:3vw;
  border-radius: 50%;
  background-color: transparent;
`;

const LeafButton = styled.button`
  background-image: url(leaf.png);
  background-size: 100% 100%;
  background-position: center center;
  background-repeat: no-repeat;
  outline: 0;
  border: 0;
  cursor: pointer;
  height: 12vh;
  width: 10vh;
  margin: 0;
  padding: 0;
  margin-top: -5vh;
  margin-left:3vw;
  margin-right:3vw;
  border-radius: 50%;
  background-color: transparent;
`;

const LeafImg = styled.img`
  position: absolute;
  z-index: -1;
  margin-left: -4.5vw;
  margin-top: -1.5vw;
`;
const ButtonText = styled(Body1)`
  font-size: 1.3vw;
  text-align: center;
  color: white;
  font-weight: 800;
`;

const ErrorText = styled(Body1)`
  font-size: 1vw;
  text-align: center;
  color: red;
  padding: 0;
  margin: 0;
  font-weight: 600;
`;

const SuccessText = styled(Body1)`
  font-size: 1vw;
  text-align: center;
  color: green;
  padding: 0;
  margin-top: 15px;
  font-weight: 700;
`;

const ButtonHolder = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  width:48vw;
  justify-content:center;
`;
const HeadingDiv = styled.div`
  margin-top: 2vh;
`;

const SendResults = () => {
  const history = useHistory();
  const state = React.useContext(Context);
  const [email, setEmail] = React.useState("");
  const inputRef = React.useRef(null);
  const [agreed, toggleAgreed] = React.useState(false);
  const [isDisable, setIsDisable] = React.useState(false);
  const [errorMessages, setErrorMessages] = React.useState([]);
  const [responseMessage, setResponseMessage] = React.useState(null);

  document.body.style.overflow = "hidden";

  React.useEffect(()=>{
    setTimeout(()=>{
      if(state.matchedOrangutan==""){
        // history.push('/user')
      }
    }, 1000)
  }, [])

  const sendEmail = async (e) => {
    e.preventDefault()
    console.log("click")
    setIsDisable(true)
    if(agreed){
      setErrorMessages([])
      setResponseMessage(null)
      const response = await post_request_json('/api/users/personality-quiz/send-result', {email, info: state.matchedOrangutan})
      console.log(response)
      if(response.status){
        setResponseMessage(response.msg)
        setTimeout(()=>{
          history.push('/user')
        }, 2000)
        setIsDisable(false)
      }else{
        setErrorMessages(response.errors)
        setIsDisable(false)
      }
    }else{
      alert("you shoud agree with our privacy and policy")
      setIsDisable(false)
    }
  }

  return (
    <Layout fullLeaves background={1}>
      <Wrapper>
        <HeadingDiv>
          <Heading2>GREAT CHOICE!</Heading2>
          <Heading4>SEND YOUR RESULTS TO LEARN MORE</Heading4>
          {responseMessage!=null && (<SuccessText>{responseMessage}</SuccessText>)}
        </HeadingDiv>
        <Container>
          <Section>
            <Input
              placeholder="E-mail address"
              placeholderTextColor="#00833D"
              type="text"
              ref={inputRef}
              onChange={(event) => setEmail(event.target.value)}
            />
            {/**** 
            {errorMessages.map((errorInfo, index)=> <ErrorText key={index}>{errorInfo.msg}</ErrorText>)}
         */}
            <Keyboard inputRef={inputRef} />
          </Section>
          <Section>
            <Heading5>PRIVACY POLICY</Heading5>
            <PrivacyNotice>
              <Para>
                By entering your e-mail address you consent to receive future
                communications from Zoo Atlanta. We won’t sell your information
                to any third parties and we need this information to provide
                goods and services that you request and to let you know of
                additional goods and services about which you may be interested.
                In addition, we use your personally identifying information to
                improve our marketing, development and promotional efforts; to
                statistically analyze site usage; and to improve our content
                offerings and to customize our site’s content, layout and
                services. We may also use your information to deliver additional
                information to you that is intended to target your identified
                interests; in particular, we may use your email address, mailing
                address or phone number to contact you regarding administrative
                notices, offerings and communications relevant to you on the
                website
              </Para>
              <Para>
                By entering your e-mail address you consent to receive future
                communications from Zoo Atlanta. We won’t sell your information
                to any third parties and we need this information to provide
                goods and services that you request and to let you know of
                additional goods and services about which you may be interested.
                In addition, we use your personally identifying information to
                improve our marketing, development and promotional efforts; to
                statistically analyze site usage; and to improve our content
                offerings and to customize our site’s content, layout and
                services. We may also use your information to deliver additional
                information to you that is intended to target your identified
                interests; in particular, we may use your email address, mailing
                address or phone number to contact you regarding administrative
                notices, offerings and communications relevant to you on the
                website
              </Para>
            </PrivacyNotice>
            <CheckboxHolder>
              <Checkbox type="checkbox" onClick={() => toggleAgreed(!agreed)} />
              <Heading5>I AGREE</Heading5>
            </CheckboxHolder>
            <ButtonHolder>
              <Link to="/user">
                <LeafButtonInverted>
                  {/* <LeafImg
                    src="leaf-inverted.png"
                    alt="leaf-button"
                    width="10%"
                  /> */}
                  <div style={{ margin: "auto" }}>
                    <ButtonTextSmall>RETURN TO MENU</ButtonTextSmall>
                  </div>
                </LeafButtonInverted>
              </Link>
              <LeafButton>
                  {/* <LeafImg src="leaf.png" alt="leaf-button" width="10%" /> */}
                  <div style={{ margin: "auto" }}>
                    <ButtonTextSmall onClick={sendEmail} disabled={isDisable}>SEND</ButtonTextSmall>
                  </div>
                </LeafButton>
            </ButtonHolder>
          </Section>
        </Container>
      </Wrapper>
    </Layout>
  );
};

export default SendResults;
