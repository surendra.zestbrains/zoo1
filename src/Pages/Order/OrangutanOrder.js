import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { H4 } from "../../Components/Fonts";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  gap: 10%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Fruit = styled.img`
  position: absolute;
  width: ${(props) => `${props.width}`};
  cursor: ${(props) => `${props.cursor}`};
  border-radius: 100px;
  top: ${(props) => `${props.top}`};
  left: ${(props) => `${props.left}`};
`;

const fruitTypes = [
  "strawberry.png",
  "watermelon.png",
  "grapes.png",
  "orange.png",
  "pineapple.png",
  "banana.png",
];

const fruitAnswers = [
  "banana.png",
  "orange.png",
  "grapes.png",
  "pineapple.png",
  "watermelon.png",
  "strawberry.png",
];

const orangutanFruitSelectedPositions = [
  { left: "5vw", top: "10vh" },
  { left: "20vw", top: "10vh" },
  { left: "35vw", top: "10vh" },
  { left: "50vw", top: "10vh" },
  { left: "65vw", top: "10vh" },
  { left: "80vw", top: "10vh" },
];

const orangutanFruitDefaultPositions = [
  { left: "5vw", top: "65vh" },
  { left: "20vw", top: "65vh" },
  { left: "35vw", top: "65vh" },
  { left: "50vw", top: "65vh" },
  { left: "65vw", top: "65vh" },
  { left: "80vw", top: "65vh" },
];

const OrangutanOrder = () => {
  // const [userFruitPositions, setUserFruitPositions] = React.useState([]);
  const [orangutanFruitPositions, setOrangutanFruitPositions] = React.useState(
    []
  );
  // const [orangutanWin, setOrangutanWin] = React.useState(false);
  const [startGame, toggleStartGame] = React.useState(false);
  const [check, setCheck] = React.useState(true);
  // if (
  //   check === true &&
  //   startGame === true &&
  //   singlePlayer === true &&
  //   orangutanFruitPositions.length < 6
  // ) {
  //   setCheck(false);

  //   setTimeout(() => {
  //     if (orangutanFruitPositions.length < 6) {
  //       for (let i = 0; i < fruitAnswers.length; i += 1) {
  //         if (
  //           orangutanFruitPositions.find(
  //             (index) => index === fruitAnswers[i]
  //           ) === undefined
  //         ) {
  //           setOrangutanFruitPositions([
  //             ...orangutanFruitPositions,
  //             fruitAnswers[i],
  //           ]);

  //           i = 6;
  //           break;
  //         }
  //       }
  //     }
  //     setCheck(true);
  //   }, 1800);
  // }

  // if (userFruitPositions.length === 5) {
  //   setOrangutanWin(true);
  // }

  // if (orangutanFruitPositions.length === 6) {
  //   let check = true;
  //   orangutanFruitPositions.map((value, index) => {
  //     if (value !== fruitAnswers[index]) check = false;
  //   });
  //   if (check === true) {
  //     return <Redirect to="/send-results" />;
  //     // alert("Orangutan Wins");
  //   }
  // } else if (userFruitPositions.length === 6) {
  //   let check = true;
  //   userFruitPositions.map((value, index) => {
  //     if (value !== fruitAnswers[index]) check = false;
  //   });
  //   if (check === true) {
  //     return <Redirect to="/send-results" />;
  //     // console.log("User Wins");
  //   }
  // }

  return (
    <Layout background={0} ReturnToMenuTop>
      <Wrapper>
        {fruitTypes.map((fruit, index) => {
          if (
            orangutanFruitPositions.find((index) => index === fruit) ===
            undefined
          ) {
            return (
              <Fruit
                key={fruit}
                src={fruit}
                alt={fruit}
                width={"17%"}
                left={orangutanFruitDefaultPositions[index].left}
                top={orangutanFruitDefaultPositions[index].top}
                onClick={() => {
                  setOrangutanFruitPositions([
                    ...orangutanFruitPositions,
                    fruit,
                  ]);
                }}
              />
            );
          }
        })}

        {orangutanFruitPositions.map((fruit, index) => {
          return (
            <Fruit
              key={fruit}
              src={fruit}
              alt={fruit}
              width={"14%"}
              left={orangutanFruitSelectedPositions[index].left}
              top={orangutanFruitSelectedPositions[index].top}
            />
          );
        })}
      </Wrapper>
    </Layout>
  );
};
export default OrangutanOrder;
