import React, { useEffect } from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { H4 } from "../../Components/Fonts";
import { Redirect } from "react-router-dom";
import ReactPlayer from "react-player";
import { useHistory } from "react-router-dom";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  gap: 10%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Frame = styled.div`
  border: 3vw solid transparent;
  padding: 2%;
  width: 25%;
  height: 70%;
  border-image: url(vertical-frame.png) 4% 4% 4% 4% round round;
  border-width: 5%;
`;

const Heading = styled(H4)`
  color: white;
  font-size: 3vw;
  line-height: 0px;
  margin: 0%;
`;

const Fruit = styled.img`
  position: absolute;
  width: 6%;
  cursor: ${(props) => `${props.cursor}`};
  border-radius: 100px;
  top: ${(props) => `${props.top}`};
  left: ${(props) => `${props.left}`};
`;

const MemorizeButton = styled.button`
  font-family: LithosPro;
  font-size: 2vw;
  background-image: url(memorize_btn_bg.png);
  background-size: 100% 100%;
  background-color: transparent;
  border: none;
  width: 25vw;
  height: 25vw;
  position: absolute;
  left: 36.5%;
  top: 18%
`;

const MemorizeButtonTextStart = styled(H4)`
  color: white;
  font-size: 2.2vw;
  margin: 0%;
  line-height: 3vw;
  position: absolute;
  top: 9vw;
`;

const MemorizeButtonTryAgain = styled.button`
  background-image: url(divided_leaf.png);
  background-size: 100% 100%;
  background-color: transparent;
  border: none;
  width: 36vw;
  height: 16vw;
  position: absolute;
  top: 30%;
  left: 30%;
`;

const MemorizeButtonText = styled(H4)`
  color: white;
  font-size: 2vw;
  margin: 0%;
`;

const MemorizeSymbolImage = styled.img`
  position: absolute;
  width: 20%;
  left: 63.5%;
  top: 30%;
`;

const ReactPlayerStyled = styled(ReactPlayer)`
  position: absolute;
  left: 56%;
  top: 63%;
`;

let fruitTypes = [
  "banana.png",
  "grapes.png",
  "pear.png",
  "pineapple.png",
  "strawberry.png",
  "watermelon.png",
];

let fruitAnswers = ["banana.png", "pear.png", "grapes.png", "watermelon.png"];

const userFruitDefaultPositions = [
  { left: "30%", top: "75%" },
  { left: "22%", top: "75%" },
  { left: "14%", top: "75%" },
  { left: "33%", top: "66%" },
  { left: "25%", top: "66%" },
  { left: "17%", top: "66%" },
];

const userFruitSelectedPositions = [
  { left: "24%", top: "20%" },
  { left: "24%", top: "29%" },
  { left: "24%", top: "38%" },
  { left: "24%", top: "47%" },
  { left: "24%", top: "56%" },
  { left: "24%", top: "65%" },
];

const orangutanFruitSelectedPositions = [
  { left: "70%", top: "20%" },
  { left: "70%", top: "29%" },
  { left: "70%", top: "38%" },
  { left: "70%", top: "47%" },
  { left: "70%", top: "56%" },
  { left: "70%", top: "65%" },
];

// const orangutanFruitDefaultPositions = [
//   { left: "76%", top: "75%" },
//   { left: "68%", top: "75%" },
//   { left: "60%", top: "75%" },
//   { left: "79%", top: "66%" },
//   { left: "71%", top: "66%" },
//   { left: "63%", top: "66%" },
// ];

//Randomise an array elements
const shuffle = (array) => {
  var currentIndex = array.length, randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex != 0) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

fruitTypes = shuffle(fruitTypes)

const OrderGame = ({ singlePlayer }) => {
  const history = useHistory();
  const [userFruitPositions, setUserFruitPositions] = React.useState([]);
  const [orangutanFruitPositions, setOrangutanFruitPositions] = React.useState(
    []
  );
  // const [orangutanWin, setOrangutanWin] = React.useState(false);
  const [startGame, toggleStartGame] = React.useState(false);
  const [check, setCheck] = React.useState(true);
  const [memorizeText, setMemorizeText] = React.useState(true);
  const [check1, setCheck1] = React.useState(true);
  const [memorizeSymbols, setMemorizeSymbols] = React.useState(false);
  const [showOrangotangVideo, setShowOrangotangVideo] = React.useState(false);
  const [check2, setCheck2] = React.useState(true);
  const [currentFruit, setCurrentFruit] = React.useState(0);
  const [selectedFruits, setSelectedFruits] = React.useState(0);
  const [restartGame, setRestartGame] = React.useState(false);
  const [userFruitDisable, setUserFruitDisable] = React.useState(false);

  useEffect(() => {
    if (singlePlayer != true) {
      fruitAnswers = shuffle(fruitAnswers)
    }
  }, [])

  //SETTING TIMEOUT FOR AUTO-PLAY ON ORANGUTAN SIDE
  if (
    check === true &&
    startGame === true &&
    singlePlayer === true &&
    orangutanFruitPositions.length < 4
  ) {
    setCheck(false);

    setTimeout(() => {
      if (orangutanFruitPositions.length < 4) {
        for (let i = 0; i < fruitAnswers.length; i += 1) {
          if (
            orangutanFruitPositions.find(
              (index) => index === fruitAnswers[i]
            ) === undefined
          ) {
            setOrangutanFruitPositions([
              ...orangutanFruitPositions,
              fruitAnswers[i],
            ]);

            i = 4;
            break;
          }
        }
      }
      setCheck(true);
    }, 5500);
  }
  //SETTING TIMEOUT FOR THE INITIAL MEMORIZE TEXT BUTTON
  if (check1 === true) {
    setTimeout(() => {
      if (check1 === true && memorizeText === true) {
        setCheck1(false);
        setMemorizeText(false);
        setMemorizeSymbols(true);
      }
    }, 3000);
  }

  //SETTING TIMEOUT FOR THE DISPLAYING VIDEO
  const [displayVideo, setDisplayVideo] = React.useState(false);
  if (displayVideo === false) {
    setTimeout(() => {
      setDisplayVideo(true);
      toggleStartGame(!startGame);
    }, 11000);
  }
  //SETTING TIMEOUT FOR THE SYMBOLS TO MEMORIZE (0.5s)
  if (check2 === true && memorizeSymbols === true) {
    setCheck2(false);
    setTimeout(() => {
      if (currentFruit < 4) {
        setCurrentFruit(currentFruit + 1);
      } else {
        setMemorizeSymbols(false);
        setShowOrangotangVideo(true)
      }
      setCheck2(true);
    }, 1000);
  }

  const restartYourGameAgain = () => {
    sessionStorage.setItem("restartCounter", parseInt(sessionStorage.restartCounter) + 1);
    history.go(0)
  }

  // if (userFruitPositions.length === 5) {
  //   setOrangutanWin(true);
  // }
  if (orangutanFruitPositions.length === 4) {
    let check = true;
    orangutanFruitPositions.map((value, index) => {
      if (value !== fruitAnswers[index]) check = false;
    });
    if (check === true) {
      setTimeout(() => {
        if (typeof window !== `undefined`) {
          if (!sessionStorage.restartCounter) {
            sessionStorage.restartCounter = 0
          }

          if (parseInt(sessionStorage.restartCounter) <= 2) {
            if (JSON.stringify(userFruitPositions) !== JSON.stringify(fruitAnswers)) {
              setRestartGame(true)
            }
          } else {
            sessionStorage.removeItem("restartCounter");
            setTimeout(() => {
              history.replace('send-results');
            }, 3000)
          }
        }
      }, 2000);
      // alert("Orangutan Wins");
    }
  } else if (userFruitPositions.length === 4) {
    let check = true;
    userFruitPositions.map((value, index) => {
      if (value !== fruitAnswers[index]) check = false;
    });

    if (check === true) {
      sessionStorage.removeItem("restartCounter");
      setTimeout(() => {
        history.replace('send-results');
      }, 3000)
      // console.log("User Wins");
    }
  }

  return (
    <Layout background={1}>
      <Wrapper>
        {restartGame === true ? (
          <MemorizeButtonTryAgain onClick={() => restartYourGameAgain()}>
            <MemorizeButtonText>TRY AGAIN</MemorizeButtonText>
          </MemorizeButtonTryAgain>
        ) : ''
        }
        {fruitTypes.map((fruit, index) => {
          if (
            userFruitPositions.find((index) => index === fruit) === undefined
          ) {
            return (
              <Fruit
                key={fruit}
                src={fruit}
                alt={fruit}
                cursor={"pointer"}
                left={userFruitDefaultPositions[index].left}
                top={userFruitDefaultPositions[index].top}
                disabled={userFruitDisable}
                onClick={() => {
                  if (selectedFruits < 4 && !memorizeSymbols) {
                    if (fruit != fruitAnswers[selectedFruits]) {
                      setUserFruitDisable(true)
                      setRestartGame(true)
                    }
                    if (!userFruitDisable) {
                      setUserFruitPositions([...userFruitPositions, fruit]);
                      setSelectedFruits(selectedFruits + 1);
                    }
                  }
                }}
              />
            );
          }
        })}
        {userFruitPositions.map((fruit, index) => {
          return (
            <Fruit
              key={fruit}
              src={fruit}
              alt={fruit}
              left={userFruitSelectedPositions[index].left}
              top={userFruitSelectedPositions[index].top}
            />
          );
        })}

        {/* {singlePlayer?fruitTypes.map((fruit, index) => {
          if (
            orangutanFruitPositions.find((index) => index === fruit) ===
            undefined
          ) {
            return (
              <Fruit
                key={fruit}
                src={fruit}
                alt={fruit}
                left={orangutanFruitDefaultPositions[index].left}
                top={orangutanFruitDefaultPositions[index].top}
              />
            );
          }
        }):null} */}

        {memorizeText ? (
          <MemorizeButton>
            <MemorizeButtonTextStart>CAN YOU REMEMBER THE ORDER?</MemorizeButtonTextStart>
          </MemorizeButton>
        ) : null}
        {memorizeSymbols && !memorizeText ? (
          <MemorizeSymbolImage src={fruitAnswers[currentFruit]} />
        ) : null}
        {/* {singlePlayer && !memorizeText && displayVideo ? (
          <ReactPlayerStyled
            width={"33vw"}
            height={"26vh"}
            onEnded={() => {
              // toggleStartGame(!startGame);
            }}
            playing
            url={"sequencing-keyboard-replacement.mp4"}
          />
        ) : null} */}
        {orangutanFruitPositions.map((fruit, index) => {
          return (
            <Fruit
              key={fruit}
              src={fruit}
              alt={fruit}
              left={orangutanFruitSelectedPositions[index].left}
              top={orangutanFruitSelectedPositions[index].top}
            />
          );
        })}
        <Frame>
          <Heading>YOU</Heading>
        </Frame>
        <Frame>
          <Heading>ORANGUTAN</Heading>
          {!memorizeSymbols && showOrangotangVideo && singlePlayer ? (<ReactPlayerStyled
            width={"33vw"}
            height={"26vh"}
            onEnded={() => {
              // toggleStartGame(!startGame);
            }}
            playing
            url={"https://zoo-atlanta-new.s3.ap-south-1.amazonaws.com/order-game-orangutan.mp4"}
          />) : null}
        </Frame>
      </Wrapper>
    </Layout>
  );
};

export default OrderGame;