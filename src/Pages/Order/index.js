import React from "react";
import styled from "styled-components";
import Layout from "../../Components/Layout";
import { Link, Redirect } from "react-router-dom";
import ReactPlayer from "react-player";
import { useHistory } from "react-router-dom";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const LinkButton = styled(Link)`
  postion: absolute;
  width: 600px;
  position: absolute;
  text-align: center;
  margin: auto;
`;

const Frame = styled.img`
  z-index: 1000;
  margin-top: 3%;
  width: 700px;
`;

const ReactPlayerStyled = styled(ReactPlayer)`
  position: relative;
  margin-left: -4%;
`;

// const Art = styled.img`
//   width: 600px;
//   position: absolute;
//   text-align: center;
//   margin: -30% 0% 0% -50%;
// `;
const OrderGame = (singlePlayer) => {
  const history = useHistory();
  const [redirect, toggleRedirect] = React.useState(false);

  const OrderGameRedirect = () => {
    history.replace('order-game');
  }

  if (redirect && singlePlayer) return <Redirect to="/order-game" />;
  if (redirect && !singlePlayer)
    return <Redirect to="/order-game-multiplayer" />;
  else
    return (
      <Layout halfLeaves grass grassVariant={1} logo background={1}>
        <Wrapper>
          <Frame src="video_frame.png" onClick={OrderGameRedirect} alt="wood-frame" />
          <LinkButton to="/order-game">
            <ReactPlayerStyled onClick={OrderGameRedirect}
              height={"40%"}
              onEnded={() => {
                toggleRedirect(!redirect);
              }}
              playing
              url={"https://zoo-atlanta-new.s3.ap-south-1.amazonaws.com/thinking-apes-video.mp4"}
            />
          </LinkButton>
        </Wrapper>
      </Layout>
    );
};

export default OrderGame;
