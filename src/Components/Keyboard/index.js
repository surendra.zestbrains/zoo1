import React from "react";
import styled from "styled-components";
import { BsBackspace, BsShift } from "react-icons/bs";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 1vw;
`;
const Button = styled.button`
  outline: 0;
  border: 0;
  background: ${(props) => props.color};
  color: #ffffff;
  height: 6vh;
  width: ${(props) => props.width};
  min-width: 40px;
  cursor: pointer;
  padding: 10px;
  font-size: 2vw;
  border-radius: 8px;
`;
const Row = styled.div`
  display: flex;
  flex-direction: row;
  gap: 0.5vw;
`;

const keyboardRows = [
  ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
  ["A", "S", "D", "F", "G", "H", "J", "K", "L"],
  ["shift", "Z", "X", "C", "V", "B", "N", "M", "backspace"],
  ["123", ".", "@", ".com", "!#$"],
];

const numericKeyboardRows = [
  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
  ["!", "@", "#", "$", "%", "^", "&", "*", "("],
  ["shift", "{", "}", "=", "+", "-", "_", ")", "backspace"],
  ["abc", ".", "@", ".com", "!#$"],
];

const symbolicKeyboardRows = [
  ["!", "@", "#", "$", "%", "^", "&", "*", "(", ")"],
  ["_", "-", "+", "=", "[", "]", "{", "}", ":"],
  ["shift", "\\", "/", "<", ">", ",", "'", ";", "backspace"],
  ["123", ".", "@", ".com", "abc"],
];

const keyboards = [keyboardRows, numericKeyboardRows, symbolicKeyboardRows];

const Keyboard = ({ inputRef }) => {
  const [capslock, setCapslock] = React.useState(true);
  const [activeKeyboard, setActiveKeyboard] = React.useState(0);
  const buttonTapHandler = (e) => {
    if (e === "shift") {
      setCapslock(!capslock);
    } else if (e === "backspace") {
      inputRef.current.value = inputRef.current.value.substring(
        0,
        inputRef.current.value.length - 1
      );
    } else if (e === "abc") {
      setActiveKeyboard(0);
    } else if (e === "123") {
      setActiveKeyboard(1);
    } else if (e === "!#$") {
      setActiveKeyboard(2);
    } else inputRef.current.value = inputRef.current.value + e;
  };
  return (
    <Wrapper>
      {keyboards[activeKeyboard].map((row, index) => {
        let color = "#327830";
        let width = "max-content";
        if (index === 3) {
          color = "#20591f";
          width = "3vw";
        }
        return (
          <Row key={row}>
            {row.map((key) => {
              if (key === "backspace")
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(key);
                    }}
                    width={"3vw"}
                    color={"#20591f"}
                    key={key}
                  >
                    <BsBackspace />
                  </Button>
                );
              else if (key === "shift")
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(key);
                    }}
                    width={"3vw"}
                    color={"#20591f"}
                    key={key}
                  >
                    <BsShift />
                  </Button>
                );
              else if (key === ".com")
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(key);
                    }}
                    width={"7.25vw"}
                    color={color}
                    key={key}
                  >
                    {key}
                  </Button>
                );
              else if (key === "!#$")
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(key);
                    }}
                    width={"7.25vw"}
                    color={color}
                    key={key}
                  >
                    {key}
                  </Button>
                );
              else if (key === "123")
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(key);
                    }}
                    width={"7.25vw"}
                    color={color}
                    key={key}
                  >
                    {key}
                  </Button>
                );
              else if (key === "abc")
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(key);
                    }}
                    width={"7.25vw"}
                    color={color}
                    key={key}
                  >
                    {key}
                  </Button>
                );
              else
                return (
                  <Button
                    onClick={(e) => {
                      buttonTapHandler(capslock ? key : key.toLowerCase());
                    }}
                    width={width}
                    color={color}
                    key={key}
                  >
                    {capslock ? key : key.toLowerCase()}
                  </Button>
                );
            })}
          </Row>
        );
      })}
    </Wrapper>
  );
};

export default Keyboard;
