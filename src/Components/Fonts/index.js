import styled from 'styled-components';

export const H1 = styled.h1`
font-family: LithosPro;
  font-size: 120px;
  line-height: 150px;
`;
export const H2 = styled.h2`
font-family: LithosPro;
  font-size: 90px;
  line-height: 110px;
`;
export const H3 = styled.h3`
font-family: LithosPro;
  font-size: 60px;
  line-height: 80px;
`;
export const H4 = styled.h4`
font-family: LithosPro;
  font-size: 40px;
  line-height: 60px;
`;
export const Body1 = styled.p`font-family: LithosPro;`;
export const Body2 = styled.p`font-family: LithosPro;`;