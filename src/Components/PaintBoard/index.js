import React from "react";
import styled from "styled-components";

const PaintingContainer = styled.div`
  background: black;
  width: 100%;
  height: 100%;
`;
const PaintSwab = styled.img`
  position: absolute;
  pointer-events: none;
  width: ${(props) => `${props.width}`};
  top: ${(props) => `${props.top}`};
  left: ${(props) => `${props.left}`};
`;

const PaintBoard = ({ paintColor, brushSize, resetPaint, setResetPaint }) => {
  const [paintSwabs, setPaintSwabs] = React.useState([]);
  const [mouseDown, setMouseDown] = React.useState(false);
  React.useEffect(() => {
    if (resetPaint === true) {
      setPaintSwabs([]);
      setResetPaint(false);
    }
  }, [resetPaint]);

  return (
    <PaintingContainer
      onMouseDown={(e) => {
        setMouseDown(true);
      }}
      onMouseMove={(e) => {
        if (mouseDown === true) {
          console.log(e);
          setPaintSwabs([
            ...paintSwabs,
            {
              paintColor,
              top: `${e.clientY}px`,
              left: `${e.clientX}px`,
            },
          ]);
        }
      }}
      onMouseLeave={(e) => {
        setMouseDown(false);
      }}
      onMouseUp={(e) => {
        setMouseDown(false);
      }}
      onTouchMove={(e) => {
        setPaintSwabs([
          ...paintSwabs,
          {
            paintColor,
            top: `${e.touches[0].clientY}px`,
            left: `${e.touches[0].clientX}px`,
          },
        ]);
      }}
    >
      {paintSwabs.map((swab) => {
        return (
          <PaintSwab
            src={swab.paintColor}
            width={brushSize}
            left={swab.left}
            top={swab.top}
          />
        );
      })}
    </PaintingContainer>
  );
};
export default PaintBoard;
