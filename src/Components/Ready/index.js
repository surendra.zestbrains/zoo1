import React, { useEffect } from "react";
import styled from "styled-components";
import { H1, H2 } from "../Fonts";

const Wrapper = styled.div`
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  width: 100%;
  gap: 10px;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-start;
`;
const Heading1 = styled(H1)`
  text-align: center;
  color: #00763f;
  margin: auto;
`;
const Heading2 = styled(H2)`
  padding: 0;
  margin: 0;
  color: #215da6;
  font-size: 4vw;
  line-height: 18vh;
`;
const FrameArea = styled.div`
width: 50vw;
height: 50vh;
display: flex;
align-items: center;
justify-content: center;
`;
const Frame = styled.img`
  position: absolute;
  width: 40vw;
`;
const GoButton = styled.img`
width: 12vw;
height: 18vh;
cursor: pointer;
`;

const Ready = (props) => {
  const { toggleCountdown } = props;
  const [start, setStart] = React.useState(false);
  const [showLoading, setShowLoading] = React.useState(true);
  const [counter, setCounter] = React.useState(3);
  // React.useEffect(() => {
  //   let timer1 = setTimeout(() => setShowLoading(false), 4000);
  //   return () => {
  //     clearTimeout(timer1);
  //   };
  // }, []);

  if (start === true)
    setTimeout(() => {
      setCounter(counter - 1);
    }, 1000);
  if (counter === 0) {
    toggleCountdown(false);
  }
  return (
    <Wrapper>
      <Heading2>Ready?</Heading2>
      <FrameArea>
        {showLoading ? (
          <div
            style={{ display: "flex", flexWrap: "wrap", flexDirection: "row" }}
          >
            <div>{start ? <Heading1>3</Heading1> : null}</div>
            <div>{counter < 3 ? <Heading1>2</Heading1> : null}</div>
            <div>{counter < 2 ? <Heading1>1</Heading1> : null}</div>
          </div>
        ) : null}
        <Frame src="countdown-frame.png" alt="countdown-frame" />
      </FrameArea>
      <GoButton
        src="go-button.png"
        alt="go-button"
        onClick={() => setStart(true)}
      />
    </Wrapper>
  );
};

export default Ready;
