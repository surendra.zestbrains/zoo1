import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { H1 } from "../Fonts";
import { useHistory } from "react-router-dom";

const Logo = styled.img`
  top: 0;
  margin: auto;
  position: absolute;
  transform: translateX(-75px);
`;

const Heading1 = styled(H1)`
  text-align: left;
  color: #00763f;
  margin: auto;
  position: absolute;
  top: 0;
  left: 38vw;
`;
const Heading2 = styled(H1)`
  text-align: left;
  color: #00763f;
  margin: auto;
  position: absolute;
  top: 0;
  right: 38vw;
`;

const PongTableDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  max-width: 95vw;
  max-height: 85vh;
`;
const PongDivider = styled.img`
  height: 80vh;
  left: 0;
  width: 20vw;
  z-index: 1000;
  position: absolute;
`;
const PongStickUser = styled.img`
  height: 33.33%;
  position: absolute;
  top: ${(props) => `${props.top}px`};
  left: ${(props) => `${props.left}px`};
`;

const PongStickOrangutan = styled.img`
  height: 33.33%;
  position: absolute;
  top: ${(props) => `${props.top}px`};
  left: ${(props) => `${props.left}px`};
`;

const PongBall = styled.img`
  position: absolute;
  top: ${(props) => `${props.top}px`};
  left: ${(props) => `${props.left}px`};
`;

const TouchControlsDiv = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1000;
  width: 100vw;
  height: 100vh;
`;

const pongTypes = [
  "banana.png",
  "cantaloupe.png",
  "cherry.png",
  "grapes.png",
  "orange.png",
  "peach.png",
  "pear.png",
  "pineapple.png",
  "strawberry.png",
  "watermelon.png",
];

const pongHitSound = "sounds/pong_hit.mp3"

var timeouts = [];

const PongTable = ({ orangutanControls }) => {
  const history = useHistory();
  document.body.style.overflow = "hidden";
  const [activePong, setActivePong] = useState(0);
  const [startPongGame, setStartPongGame] = useState(true);
  const [userLeft, setUserLeft] = useState(20);
  const [userTop, setUserTop] = useState(250);
  const [orangutanTop, setOrangutanTop] = useState(20);
  const [orangutanLeft, setOrangutanLeft] = useState(window.innerWidth - 60);
  const [pongTop, setPongTop] = useState(window.innerHeight / 2);
  const [pongLeft, setPongLeft] = useState(window.innerWidth / 2);
  const [orangutanPaddlePosition, setOrangutanPaddlePosition] = useState(1);
  const [pongHorizontalDirection, setPongHorizontalDirection] = useState(-1);
  const [pongVerticalDirection, setPongVerticalDirection] = useState(0);
  const [pongVerticalGain, setPongVerticalGain] = useState(10);
  const [pongHorizontalGain, setPongHorizontalGain] = useState(10);
  const [orangutanScore, setOrangutanScore] = React.useState(0);
  const [userScore, setUserScore] = React.useState(0);
  const [timeoutCalled, setTimeoutCalled] = useState(false);


  const togglePongHorizontalDirection = () => {
    if (pongHorizontalDirection === -1) setPongHorizontalDirection(1);
    else setPongHorizontalDirection(-1);
  };

  const togglePongVerticalDirection = () => {
    if (pongVerticalDirection === -1) setPongVerticalDirection(1);
    else setPongVerticalDirection(-1);
  };

  const toggleActivePong = () => {
    if (activePong < pongTypes.length-1) setActivePong(activePong + 1);
    else setActivePong(0);
  };

  const toggleTimeoutCalled = () => {
    setTimeoutCalled(!timeoutCalled);
  };
  const incrementOrangutanPaddlePosition = () => {
    if (orangutanPaddlePosition === 2) setOrangutanPaddlePosition(0);
    else setOrangutanPaddlePosition(orangutanPaddlePosition + 1);
  };
  // FUNCTION MOVES THE PONG AROUND ACCORDING TO VERTICAL/HORIZONTAL GAINS AND DIRECTIONS
  const pongMovement = () => {
    if (pongTop >= 0 && pongVerticalDirection === -1) {
      setPongTop(pongTop - pongVerticalGain);
    }
    if (
      pongTop <= window.innerHeight - window.innerHeight * 0.1 &&
      pongVerticalDirection === 1
    ) {
      setPongTop(pongTop + pongVerticalGain);
    }

    if (pongLeft >= 0 && pongHorizontalDirection === -1) {
      setPongLeft(pongLeft - pongHorizontalGain);
    }
    if (
      pongLeft <= window.innerWidth - window.innerWidth * 0.05 &&
      pongHorizontalDirection === 1
    ) {
      setPongLeft(pongLeft + pongHorizontalGain);
    }
    if (timeouts.length > 1000) {
      for (var i = 0; i < timeouts.length; i++) {
        clearTimeout(timeouts[i]);
      }
      timeouts = [];
    }
  };

  // Function stop the game and redirect to trainer menu
  const stopGameAndRedirect = () => {
      setStartPongGame(false)
      setTimeout(()=>{
        history.push('/trainer-experience')
      }, 3000)
  }

  //Play sound in pong games
  const playSound = (soundPath) => {
    const audio = new Audio(soundPath);
    audio.play();
  }

  // FUNCTIONS FOR INCREMENTING SCORES TO BE CALLED ON HORIZONTAL WALL COLLISIONS
  const incrementOrangutanScore = () => {
    if(orangutanScore<3){
      setOrangutanScore(orangutanScore + 1)
    }
    if(orangutanScore==2){
      stopGameAndRedirect()
    } 
  };

  const incrementUserScore = () => {
    if(userScore<3){
      setUserScore(userScore + 1)
    }
    if(userScore==2){
      stopGameAndRedirect()
    } 
  };

  // FUNCTION SWITCHES HORIZONTAL AND VERTICAL POSITIONS OF PONG ON COLLISION (FOR NOW IT WORKS ON WALLS)
  const pongCollision = () => {
    if (pongLeft <= 0) {
      setPongLeft(window.innerWidth / 2);
      setPongTop(window.innerHeight / 2);
      incrementOrangutanScore();
      return false;
    } else if (pongLeft >= window.innerWidth - window.innerWidth * 0.1) {
      setPongLeft(window.innerWidth / 2);
      setPongTop(window.innerHeight / 2);
      incrementUserScore();
      return false;
    }

    if (pongTop <= 0) {
      togglePongVerticalDirection();
      setPongTop(3);
      return false;
    } else if (pongTop >= window.innerHeight - window.innerHeight * 0.1) {
      togglePongVerticalDirection();
      setPongTop(window.innerHeight - window.innerHeight * 0.1 - 3);
      return false;
    }
    return true;
  };

  const paddleCollision = () => {
    if(userScore < 3 && orangutanScore < 3){
      if (
        pongTop >= userTop &&
        pongTop <=
          userTop + (window.innerHeight - window.innerHeight * 0.1) * 0.3 &&
        pongLeft >= userLeft &&
        pongLeft <= userLeft + 10
      ) {
        if (
          pongTop <=
          userTop + ((window.innerHeight - window.innerHeight * 0.1) * 0.3) / 2
        ) {
          setPongVerticalDirection(-1);
        } else if (
          pongTop >
          userTop + ((window.innerHeight - window.innerHeight * 0.1) * 0.3) / 2
        ) {
          setPongVerticalDirection(1);
        }
        toggleActivePong();
        togglePongHorizontalDirection();
        // togglePongVerticalDirection();
        setPongLeft(userLeft + 15);
        playSound(pongHitSound)
      } 
    }
  };

  const orangutanPaddleCollision = () => {
    if(userScore < 3 && orangutanScore < 3){
    if (
      pongTop >= orangutanTop &&
      pongTop <=
        orangutanTop + (window.innerHeight - window.innerHeight * 0.1) * 0.3 &&
      pongLeft <= window.innerWidth - window.innerWidth * 0.1 &&
      pongLeft >= window.innerWidth - window.innerWidth * 0.1 - 10
    ) {
      if (
        pongTop <=
        orangutanTop +
          ((window.innerHeight - window.innerHeight * 0.1) * 0.3) / 2
      ) {
        setPongVerticalDirection(-1);
      } else if (
        pongTop >
        orangutanTop +
          ((window.innerHeight - window.innerHeight * 0.1) * 0.3) / 2
      ) {
        setPongVerticalDirection(1);
      }
      toggleActivePong();
      togglePongHorizontalDirection();
      // togglePongVerticalDirection();
      setPongLeft(window.innerWidth - window.innerWidth * 0.1 -15);
      playSound(pongHitSound)
    }
  }
  };

  /*
  var timeouts = [];
  //then, store when you create them
  timeouts.push( setTimeout( { ... }, 1000) );
  Then when you want to clear them:

  for (var i = 0; i < timeouts.length; i++) {
      clearTimeout(timeouts[i]);
  }
  //quick reset of the timer array you just cleared
  timeouts = [];
  */
  // ONLY CALLS A NEW TIMEOUT IF THERE IS A CHANGE IN STATE
  const [check, setCheck] = React.useState(true);
  if (check === true) {
    setCheck(false);
    setTimeout(() => {
      if (pongCollision()) {
        pongMovement();
        // console.log("Pong Top: ", pongTop);
        //  console.log("Pong Left: ", pongLeft);
        // console.log("User Top: ", userTop);
        // console.log("User Bottom:", userTop + 1000);
        // console.log("User Left: ", userLeft);
        // console.log("orangutan Top:", orangutanTop );
        // console.log("orangutan Bottom:", orangutanTop + 1000);
        //  console.log("orangutan Left: ", orangutanLeft);
        paddleCollision();
        orangutanPaddleCollision();
      }
      setCheck(true);
    }, 30);
  }
  // useEffect(() => {
  //   timeouts.push(
  //     setTimeout(() => {
  //       if (pongCollision()) {
  //         pongMovement();
  //         // console.log("Pong Top: ", pongTop);
  //         // console.log("Pong Left: ", pongLeft);
  //         // console.log("Horizontal Gain: ", pongHorizontalGain);
  //         // console.log("Vertical Gain:", pongVerticalGain);
  //         // console.log("Pong Horizontal Direction: ", pongHorizontalDirection);
  //         // console.log("Pong Vertical Direction: ", pongVerticalDirection);
  //         // console.log("Max Width: ", window.innerWidth);
  //         // console.log("Max Height: ", window.innerHeight);
  //       }
  //       toggleTimeoutCalled();
  //     }, 50)
  //   );
  // }, [timeoutCalled]);
  // if (pongCollision() && timeoutCalled===false) {
  //   timeouts.push(
  //     setTimeout(() => {
  //       setTimeoutCalled(true);
  //       pongMovement();
  //       // console.log("Pong Top: ", pongTop);
  //       // console.log("Pong Left: ", pongLeft);
  //       // console.log("Horizontal Gain: ", pongHorizontalGain);
  //       // console.log("Vertical Gain:", pongVerticalGain);
  //       // console.log("Pong Horizontal Direction: ", pongHorizontalDirection);
  //       // console.log("Pong Vertical Direction: ", pongVerticalDirection);
  //       // console.log("Max Width: ", window.innerWidth);
  //       // console.log("Max Height: ", window.innerHeight);
  //       setTimeoutCalled(false);
  //     }, 100)
  //   );
  // }
  const [startY, setStartY] = React.useState(0);
  const [startZ, setStartZ] = React.useState(0);
  const [mouseDown, setMouseDown] = React.useState(false);
  const [mouseDown1, setMouseDown1] = React.useState(false);
  return (
    <div>
    <div style={{position: "absolute",top: 0,left:0,zIndex: 1000,width:"50%",height: '100vh'}}
    onTouchMove={(e) => {
      const currentY = e.touches[0].clientY;
      if (
        currentY > startY &&
        userTop < window.innerHeight - window.innerHeight * 0.334
      ) {
        setUserTop(userTop + 15);
      } else if (currentY < startY && userTop > 5) {
        setUserTop(userTop - 15);
      }
      setStartY(currentY);
    }
  }


onMouseDown={(e) => {
  setMouseDown1(true);
  if (orangutanControls === false) {
    setStartY(e.clientY);
  } else if (orangutanControls === true) {
    incrementOrangutanPaddlePosition();
    if (orangutanPaddlePosition === 0) {
      setOrangutanTop(5);
    } else if (orangutanPaddlePosition === 1) {
      setOrangutanTop(window.innerHeight * 0.333333);
    } else if (orangutanPaddlePosition === 2) {
      setOrangutanTop(window.innerHeight * 0.666666);
    }
  }
}}
onMouseMove={(e) => {
  if (mouseDown1 === true) {
    if (orangutanControls === false) {
      const currentY = e.clientY;
      if (
        currentY > startY &&
        userTop < window.innerHeight - window.innerHeight * 0.334
      ) {
        setUserTop(userTop + 15);
      } else if (currentY < startY && userTop > 5) {
        setUserTop(userTop - 15);
      }
      setStartY(currentY);
    }
  }
}}


onMouseLeave={(e) => {
  setMouseDown1(false);
}}
onMouseUp={(e) => {
  setMouseDown1(false);
}}
    />

    <div style={{position: "absolute",top: 0,zIndex: 1000,width:"50%",height: '100vh'}}
  
    onTouchMove={(e) => {
      const currentZ = e.touches[0].clientY;
        if (
          currentZ > startZ &&
          orangutanTop < window.innerHeight - window.innerHeight * 0.334
        ) {
          setOrangutanTop(orangutanTop + 15);
        } else if (currentZ < startZ && orangutanTop > 5) {
          setOrangutanTop(orangutanTop - 15);
        }
        setStartZ(currentZ);
      }
    }
  onMouseDown={(e) => {
    setMouseDown(true);
    if (orangutanControls === false) {
      setStartY(e.clientY);
    } else if (orangutanControls === true) {
      incrementOrangutanPaddlePosition();
      if (orangutanPaddlePosition === 0) {
        setOrangutanTop(5);
      } else if (orangutanPaddlePosition === 1) {
        setOrangutanTop(window.innerHeight * 0.333333);
      } else if (orangutanPaddlePosition === 2) {
        setOrangutanTop(window.innerHeight * 0.666666);
      }
    }
  }}
  onMouseMove={(e) => {
    if (mouseDown === true) {
      if (orangutanControls === false) {
        const currentZ = e.clientY;
        if (
          currentZ > startZ &&
          orangutanTop < window.innerHeight - window.innerHeight * 0.334
        ) {
          setOrangutanTop(orangutanTop + 15);
        } else if (currentZ < startZ && orangutanTop > 5) {
          setOrangutanTop(orangutanTop - 15);
        }
        setStartZ(currentZ);
      }
    }
  }}
  
  
  onMouseLeave={(e) => {
    setMouseDown(false);
  }}
  onMouseUp={(e) => {
    setMouseDown(false);
  }}
    />

   
      <Heading1>{userScore}</Heading1><Logo src="zoo-logo-small.png" alt="zoo-logo" width="150px" /><Heading2>{orangutanScore}</Heading2>

      <PongDivider src="pong-divider.png" alt="pong-divider" />
      <PongTableDiv>
        <PongStickUser
          src="pong-stick.png"
          alt="pong-stick-1"
          id="stick-user"
          left={userLeft}
          top={userTop}



        />
        {startPongGame ? (
          <PongBall
            src={pongTypes[activePong]}
            top={pongTop}
            left={pongLeft}
            width={"10%"}
          />
          ):''}  
        <PongStickOrangutan
          src="pong-stick.png"
          alt="pong-stick-2"
          id="stick-Orangutan"
          left={orangutanLeft}
          top={orangutanTop}
        />
      </PongTableDiv>
    </div>
  );
};

export default PongTable;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////PONG GAME/////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

// const Loop = function(func) {
//   (function loop(time) {
//      func(Math.min((Date.now() - time) / 10, 1));
//      window.requestAnimationFrame(loop.bind(Date.now()));
//   })(Date.now());
// };

// const gameStartHeight = R.subtract(R.divide(window.innerHeight, 2), 150);
// const paddleLimit = window.innerHeight - 150;

// class Instructions extends React.Component {
//   static propTypes = {
//      clickHandler: PropTypes.func.isRequired,
//      showInstructions: PropTypes.bool.isRequired
//   }

// render() {
//      const clx = this.props.showInstructions ? 'instr': 'instr-hide';
//   return (
//            <div className={`${clx}`}>
//               <h3 style={{ textAlign: "center", marginBottom: 50 }}>Use ↑ and ↓ arrows to move the paddle.</h3>
//             <button onClick={this.props.clickHandler} className='btn'>got it</button>
//            </div>
//   )
// }
// }
// class Header extends React.Component {
//   static propTypes = {
//      clickHandler: PropTypes.func.isRequired,
//      showButton: PropTypes.bool.isRequired
//   };
//   renderPlayButton = () => {
//      const clx = this.props.showButton ? '' : '-hide';
//      return (<button className={`btn${clx}`} disabled={!this.props.showButton} onClick={this.props.clickHandler}>
//                  Play Game
//               </button>)}
//   render() {
//      return (
//         <div className="Header">
//            <div className="menu">
//               <h1>Welcome to Pong</h1>
//               <div className="btnWrapper">
//                  {this.renderPlayButton()}
//               </div>
//            </div>
//         </div>
//      );
//   }
// }

// class Score extends React.Component {
//   static propTypes = {
//      total: PropTypes.number,
//      position: PropTypes.oneOf(["left", "right"]).isRequired,
//      player: PropTypes.string.isRequired
//   };

//   static defaultProps = {
//      total: 0
//   };

//   render() {
//      return (
//         <div className={this.props.position}>
//            <h2>Player {this.props.player}</h2>
//            <h2>{this.props.total}</h2>
//         </div>
//      );
//   }
// }

// class PongBall extends React.Component {
//   static propTypes = {
//      x: PropTypes.number.isRequired,
//      y: PropTypes.number.isRequired
//   };

//   render() {
//      return (
//         <div
//            style={{
//               width: "30px",
//               height: "30px",
//               top: `${this.props.y}px`,
//               left: `${this.props.x}px`,
//               position: "absolute",
//               backgroundColor: "white"
//            }}
//            className="PongBall"
//         />
//      );
//   }
// }

// class Paddle extends React.Component {
//   static propTypes = {
//      x: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
//      y: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
//      onKeyDown: PropTypes.func,
//      tabIndex: PropTypes.string
//   };

//   static defaultProps = {
//      onKeyDown: Function.prototype,
//      tabIndex: ""
//   };

//   render() {
//      return (
//         <div
//            role="button"
//            onKeyDown={this.props.onKeyDown}
//            className="Paddle"
//            tabIndex={this.props.tabIndex}
//            style={{
//               width: "15px",
//               height: "150px",
//               position: "absolute",
//               backgroundColor: "#ffffff",
//               opacity: "0.7",
//               top: `${this.props.y}px`,
//               left: `${this.props.x}px`
//            }}
//         >
//            <input type="text" className="paddleInput" />
//         </div>
//      );
//   }
// }

// class Game extends React.Component {
//   constructor(props) {
//      super(props);
//      this.state = {
//         ballY: Math.floor(window.innerHeight / 2),
//         ballX: Math.floor(window.innerWidth / 2),
//         // randomly choose the direction
//         vx: 5 * (Math.random() < 0.5 ? 1 : -1), // accelleration
//         vy: 5 * (Math.random() < 0.5 ? 1 : -1), // accelleration
//         speed: 2 * (Math.random() < 0.5 ? 1 : -1),

//         leftScore: 0,
//         rightScore: 0,

//         leftPaddleTop: gameStartHeight,
//         rightPaddleY: 150,
//         hasGameStarted: false,
//         showInstructions: true
//      };
//   }

//   onKeyDown = event => {
//      // if up arrow hit & top of paddle is below top header
//      if (
//         event.keyCode === 38 &&
//         event.target.getBoundingClientRect().top > 0
//      ) {
//         this.setState(prevState => ({
//            leftPaddleTop: prevState.leftPaddleTop - 10
//         }));
//      } else if (
//         event.keyCode === 40 &&
//         R.add(event.target.getBoundingClientRect().top, 150) <
//            window.innerHeight
//      ) {
//         // if down arrow is hit and at the bottom of the window
//         this.setState(prevState => ({
//            leftPaddleTop: prevState.leftPaddleTop + 10
//         }));
//      }
//   };

//   focusPaddle = () => {
//      ReactDOM.findDOMNode(this.refs.p1).focus();
//   };

//   startButtonHandler = e => {
//      this.setState({
//         ballX: window.innerWidth / 2,
//         ballY: window.innerHeight / 2,
//         leftScore: 0,
//         rightScore: 0,
//         hasGameStarted: true
//      });
//      e.target.blur();

//      this.focusPaddle();
//      this.startGame();
//   };

//   startGame = () => {
//      Loop(tick => {
//         tick === 0 ? (tick = 1) : tick;

//            this.easyAI(tick);

//         this.setState({
//            ballX: R.add(this.state.ballX, this.state.vx),
//            ballY: R.add(this.state.ballY, this.state.vy)
//         });

//         // // if the ball is at the right side of the screen
//         if (this.state.vx > 0 && this.state.ballX > R.add(this.boardBoundsRight, 50)) {
//            if (this.state.leftScore !== 11) {
//               this.setState({
//                  vx: R.negate(this.state.vx),
//                  leftScore: R.inc(this.state.leftScore)
//               }); // reverse direction of ball
//            } else {
//               this.setState({
//                  vx: R.negate(this.state.vx)
//               });
//            }
//         }

//         if (
//            this.state.ballX > R.subtract(this.boardBoundsRight, 50) &&
//            this.state.ballY > this.state.rightPaddleY &&
//            this.state.ballY < R.add(this.state.rightPaddleY, 150)
//         ) {
//            return this.setState({
//               vx: R.negate(this.state.vx)
//            });
//         }

//         // if ball is on the left side past paddle
//         if (this.state.vx < 0 && this.state.ballX < -50) {
//            if (this.state.rightScore !== 11) {
//               this.setState({
//                  rightScore: R.inc(this.state.rightScore),
//                  vx: R.negate(this.state.vx)
//               }); // reverse direction of ball
//            } else {
//               this.setState({ vx: R.negate(this.state.vx) });
//            }
//         } else if (
//            this.state.vx < 0 &&
//            this.state.ballX <= 20 &&
//            this.state.ballY > this.state.leftPaddleTop &&
//            this.state.ballY < R.add(this.state.rightPaddleY, 150)
//         ) {
//            // Hit left paddle!!
//            return this.setState({
//               vx: R.negate(this.state.vx)
//            });
//         }

//         // if the ball is at the bottom or top of the board
//         if (this.state.ballY > window.innerHeight - 15) {
//            this.setState({ vy: R.negate(this.state.vy) });
//         } else if (this.state.ballY < 0) {
//            this.setState({ vy: R.negate(this.state.vy) });
//         }
//      });
//   };

//   easyAI = tick => {
//      if (this.state.rightPaddleY >= 0) {
//         this.setState(prevState => ({
//            rightPaddleY: R.multiply(
//               R.add(prevState.rightPaddleY, this.state.speed),
//               tick
//            )
//         }));
//      }

//      if (this.state.rightPaddleY < 0) {
//         this.setState(prevState => ({
//            speed: R.negate(prevState.speed),
//            rightPaddleY: R.multiply(
//               R.add(prevState.rightPaddleY, R.negate(this.state.speed)),
//               tick
//            )
//         }));
//      }
//      if (this.state.rightPaddleY > R.subtract(window.innerHeight, 120)) {
//         this.setState(prevState => ({
//            speed: R.negate(prevState.speed),
//            rightPaddleY: R.multiply(
//               R.add(prevState.rightPaddleY, R.negate(this.state.speed)),
//               tick
//            )
//         }));
//      }
//   };

//   boardBoundsRight = window.innerWidth + 10;

//   renderGameOverText = () => {
//      const {
//         leftScore,
//         rightScore
//      } = this.state;
//      const winner = leftScore === 11 ? "Player 1" : "Player 2";
//      return leftScore === 11 ||
//         (rightScore === 11 && (
//            <div className="gameOver">
//               <h1 style={{ display: "flex" }}>{`game over ${winner} wins`}</h1>
//               <h3>Refresh page to play again</h3>
//            </div>
//         ));
//   };

//   closeInstructions = () => {console.log('in click handler')
//                              return this.setState({showInstructions: !this.state.showInstructions})}

//   render() {
//      const {
//         ballX,
//         ballY,
//         leftPaddleTop,
//         leftScore,
//         level,
//         rightScore,
//         hasGameStarted,
//         showInstructions
//      } = this.state;
//      const isGameOver =  leftScore === 11|| rightScore === 11
//      return (
//         <div className="Game">
//            <Header
//               level={level}
//               clickHandler={this.startButtonHandler}
//               showButton={isGameOver || !hasGameStarted}
//            />
//            <Score position="left" player="1" total={leftScore} />
//            {this.renderGameOverText()}
//            <Instructions clickHandler={this.closeInstructions} showInstructions={showInstructions}/>
//            <Score position="right" player="2" total={rightScore} />
//            <Paddle
//               name="p1"
//               id="p1"
//               x={5}
//               tabIndex="0"
//               ref="p1"
//               y={leftPaddleTop}
//               onKeyDown={this.onKeyDown}
//               position="left"
//               focused={this.state.focused}
//            />
//            <div className="vl" />
//            <PongBall x={ballX} y={ballY} />
//            <Paddle
//               x={R.subtract(this.boardBoundsRight, 30)}
//               y={this.state.rightPaddleY}
//               position="right"
//            />
//         </div>
//      );
//   }
// }

// class App extends React.Component {
//   render() {
//      return (
//         <div className="App">
//            <Game />
//         </div>
//      );
//   }
// }

// ReactDOM.render(<App />, document.getElementById("root"));
