import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Body1 } from "../../Components/Fonts/index";

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  overflow-x: hidden;
  background-repeat: no-repeat;
  background-image: url(${(props) => props.src});
  background-position: center;
  background-size: cover;
`;
const FullLeaves = styled.img`
  top: 0;
  left: 0;
  width: 100%;
  position: fixed;
`;
const HalfLeaves = styled.img`
  top: 0;
  left: 0;
  z-index: 2;
  width: 40vw;
  position: fixed;
`;
const ReturnToMenuButtonTop = styled(Body1)`
  z-index: 1000;
  font-size: 1.8vw;
  font-weight: 600;
  color: #00833d;
  cursor: pointer;
  position: absolute;
  left: 3vw;
  top: 1vh;
  width: 10vw;
`;

const ReturnToMenuButtonBottom = styled(Body1)`
  z-index: 1000;
  font-size: 1.8vw;
  font-weight: 600;
  color: #00833d;
  cursor: pointer;
  position: absolute;
  left: 3vw;
  top: 83vh;
  width: 10vw;
`;

const Orangutan = styled.img`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 24%;
  margin-left: ${(props) => props.marginLeft};
  margin-bottom: ${(props) => props.marginBottom};
  z-index: ${(props) => props.zIndex};
`;

const Grass = styled.img`
  bottom: 0;
  left: -1vw;
  z-index: 2;
  width: 103%;
  position: fixed;
  height: 5vw;
`;
const Logo = styled.img`
  top: 0;
  right: 0;
  margin-right: 1%;
  margin-top: 1%;
  width: 10%;
  position: fixed;
  z-index: 999;
`;
const Body = styled.div`
  width: 100%;
  z-index: 1;
`;
const backgrounds = [
  "background-0.png",
  "background-1.png",
  "background-2.png",
];
const grassLinks = ["grass0.png", "grass1.png", "grass2.png"];
const orangutanVariants = ["orangutan-walking.png", "orangutan-sitting.png"];
const Layout = (props) => {
  const {
    fullLeaves,
    halfLeaves,
    grass,
    grassVariant,
    logo,
    orangutanStanding,
    orangutanSitting,
    orangutanBehindGrass,
    background,
    trainer,
    ReturnToMenuBottom,
    ReturnToMenuTop
  } = props;
  let homepageLink = "/user";
  if (trainer === true) homepageLink = "/trainer";
  return (
    <Wrapper src={backgrounds[background]}>
      {fullLeaves ? <FullLeaves src="fullleaves.png" alt="leaves" /> : null}
      {halfLeaves ? (
        <HalfLeaves src="halfleaves.png" alt="leaves-small" />
      ) : null}
      {logo ? (
        <Link to={homepageLink}>
          <Logo src="zoo-logo-small.png" alt="zoo-logo" />
        </Link>
      ) : null}
      <Body>{props.children}</Body>
      {grass ? (
        <Grass src={grassLinks[grassVariant]} alt={grassLinks[grassVariant]} />
      ) : null}
      {ReturnToMenuTop ? (
        <Link to={"/trainer"}>
          <ReturnToMenuButtonTop>Return To Menu</ReturnToMenuButtonTop>
        </Link>
      ) : null}
      {ReturnToMenuBottom ? (
        <Link to={"/trainer"}>
          <ReturnToMenuButtonTop>Return To Menu</ReturnToMenuButtonTop>
        </Link>
      ) : null}
      {orangutanStanding ? (
        <Orangutan
          src={orangutanVariants[0]}
          alt="orangutan"
          zIndex="3"
          marginLeft="-4.5%"
          marginBottom="-4%"
        />
      ) : null}
      {orangutanSitting ? (
        <Orangutan
          src={orangutanVariants[1]}
          alt="orangutan"
          zIndex="3"
          marginLeft="-3%"
          marginBottom="-4%"
        />
      ) : null}
      {orangutanBehindGrass ? (
        <Orangutan
          src={orangutanVariants[0]}
          alt="orangutan"
          zIndex="1"
          marginLeft="-4.5%"
          marginBottom="-4%"
        />
      ) : null}
    </Wrapper>
  );
};

export default Layout;
